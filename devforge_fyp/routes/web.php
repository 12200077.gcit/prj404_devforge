<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Guest\GuestController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\MyProjectsController;
use App\Http\Controllers\User\ProjectScopingFormController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AddprojectController;
use App\Http\Controllers\Admin\ApproveprojectController;
use App\Http\Controllers\ApprovalTeam\ApprovalDashboardController;
use App\Http\Controllers\ApprovalTeam\AddsupervisorController;
use App\Http\Controllers\ApprovalTeam\ApproveTeamProjectController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\CreateProjectController;


// Guest route
Route::group([], function () {
    Route::get('/', [GuestController::class,'home'])->name('home');
    Route::get('/aboutus', [GuestController::class, 'aboutus']);
    Route::get('/contactus', [GuestController::class, 'contactus'])->name('contactus');
    Route::post('userFeedback', [GuestController::class, 'userFeedback'])->name('userFeedback');
    Route::get('/completedProjects', [GuestController::class, 'completedProjects']);
});

// User route
Route::group(['middleware' => ['auth','user']], function () {
    Route::get('/profile-complete', [UserController::class,'profileComplete'])->name('profile-complete');
    Route::post('/profileComplete', [UserController::class, 'userProfileComplete'])->name('userProfileComplete');

    Route::get('/myProject', [MyProjectsController::class,'myProject'])->name('myProject');
    Route::get('/applyProject/{id}', [MyProjectsController::class,'applyProject']);
    Route::post('/applyProject', [MyProjectsController::class, 'applyGroup'])->name('applyGroup');

    Route::get('/projectScopingForm/{group_id}', [ProjectScopingFormController::class,'projectScopingForm']);
    Route::post('/ScopingForm/{group_id}', [ProjectScopingFormController::class, 'postScopingForm'])->name('ScopingForm');

    // Update Profile
    Route::get('/viewProfile/{id}/edit', [UserController::class, 'editProfile'])->name('editProfile');
    Route::put('/viewProfile/{id}', [UserController::class, 'updateProfile'])->name('updateProfile');

    // Download 
    Route::get('userExport/{scoping_id}', [MyProjectsController::class, 'userExports'])->name('userExports');
});

// Admin route
Route::group(['middleware' => ['auth','admin'],'prefix' => 'admin'], function () {
    Route::get('dashboard', [DashboardController::class,'dashboard'])->name('admin.dashboard');
    Route::get('overview', [DashboardController::class,'overview'])->name('overview');
    Route::get('ongoing', [DashboardController::class,'ongoing'])->name('ongoing');
    Route::get('projectclosetoDeadline', [DashboardController::class,'close'])->name('close');

    Route::get('approveProject', [ApproveprojectController::class,'approve'])->name('approve');
    Route::post('approveProject', [ApproveprojectController::class, 'approveProject'])->name('approveProject');

    Route::get('addProject', [AddprojectController::class,'addProject'])->name('addProject');
    Route::post('addProject', [CreateProjectController::class, 'createProject'])->name('createProject');

    //Download Scoping Documents
    Route::get('adminDownloadScopingDocuments/{scoping_id}', [DashboardController::class, 'downloadScopingDocuments'])->name('downloadAdmin');

});

// approval team route
Route::group(['middleware' => ['auth','approvalteam'],'prefix' => 'approvalteam'], function () {
    Route::get('dashboard', [ApprovalDashboardController::class,'dashboard'])->name('approval.dashboard');

    Route::get('addSupervisor', [AddsupervisorController::class,'addSupervisor'])->name('addSupervisor');
    Route::post('addSupervisor', [AddsupervisorController::class, 'createSupervisor'])->name('createSupervisor');
    Route::delete('deleteSupervisor/{id}', [AddsupervisorController::class, 'deleteSupervisor'])->name('deleteSupervisor');

    Route::get('addProject', [AddsupervisorController::class,'addProject']);
    Route::post('addProject', [AddsupervisorController::class, 'approvalAddProject'])->name('approvalAddProject');

    Route::get('approveProject', [ApproveTeamProjectController::class,'approvePage'])->name('approvalPage');
    Route::post('approveProject', [ApproveTeamProjectController::class, 'approvalProject'])->name('approvalProject');
    Route::post('completeProject', [ApproveTeamProjectController::class, 'completeProject'])->name('completeProject'); 

    //Download scoping documents
    Route::get('downloadScopingDocuments/{scoping_id}', [ApproveTeamProjectController::class, 'downloadScopingDocuments'])->name('downloadScopingDocuments');
    Route::post('sendFeedback/{email}', [ApproveTeamProjectController::class, 'sendFeedback'])->name('sendFeedback');

});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';