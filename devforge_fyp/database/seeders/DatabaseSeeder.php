<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'role' => 'admin',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'created_at' => now(),
            'updated_at' => now(),

            'contact_number' => null,
            'enrollment_number' => null,
            'linkedin' => null,
            'online' => null,
            'skills' => null,
            'interest' => null,
            
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'role' => 'user',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'created_at' => now(),
            'updated_at' => now(),

            'contact_number' => null,
            'enrollment_number' => null,
            'linkedin' => null,
            'online' => null,
            'skills' => null,
            'interest' => null,
        ]);
        DB::table('users')->insert([
            'name' => 'Approval User',
            'email' => 'approval@gmail.com',
            'role' => 'approvalteam',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'created_at' => now(),
            'updated_at' => now(),

            'contact_number' => null,
            'enrollment_number' => null,
            'linkedin' => null,
            'online' => null,
            'skills' => null,
            'interest' => null,
        ]);
    }
}
