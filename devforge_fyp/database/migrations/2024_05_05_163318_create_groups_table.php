<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id('group_id');
            $table->string('project_id');
            $table->string('scoping_id')->nullable();
            $table->string('project_name');
            $table->string('team_leader');
            $table->date('applied');
            $table->json('members');
            $table->string('project_status')->default('pending');
            $table->string('admin_status')->default('pending');
            $table->string('approval_status')->default('pending');
            $table->string('scoping_status')->default("No");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('groups');
    }
};
