<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scopingdocuments', function (Blueprint $table) {
            $table->id();
            $table->string('project_id');
            $table->string('group_id');
            $table->string('scope', 3000);
            $table->string('deliverables', 3000);
            $table->string('requirements', 3000);
            $table->string('sitemap');
            $table->string('scoping_status')->default("No");

            $table->date('gathering_start');
            $table->date('gathering_end');
            $table->date('analysis_start');
            $table->date('analysis_end');
            $table->date('design_start');
            $table->date('design_end');
            $table->date('coding_start');
            $table->date('coding_end');
            $table->date('testing_start');
            $table->date('testing_end');
            $table->date('deployment_start');
            $table->date('deployment_end');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scopingdocuments');
    }
};
