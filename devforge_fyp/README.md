
# GCIT Devforge Project Bidding System

## Overview

The GCIT Devforge Project Bidding System is an innovative platform developed to streamline the project allocation and management process at GCIT. It aims to replace inefficient manual procedures with a transparent, inclusive, and automated system, enabling students to bid on industrial projects that align with their skills and interests.

## Features

- User-Friendly Interface: Intuitive design for easy navigation.
- Efficient Project Tracking: Real-time status updates, document uploads, and notifications.
- Transparent Bidding Process: Fair and unbiased project allocation.
- Comprehensive Reporting: Detailed reports on bidding activities, project progress, and user engagement.
- Secure Data Handling: Robust authentication and data protection measures.

## Modules

### Admin Module

- Authentication: Secure login for admins.
- Project Management: Add and manage projects, set deadlines, and view details.
- Bidding Management: View and manage bids, close bidding, and approve projects.
- Reporting: Access detailed reports on project status and user engagement.
- Update Password: Reset admin passwords.

### User Module

- Registration and Authentication: Sign up and login securely.
- Profile Management: Edit personal information and view project participation history.
- Project Bidding: View available projects and place bids.
- Project Management: Track project progress and upload documents.
- Communication: Receive notifications regarding project status and bidding results.
- Reporting: Access reports on bidding activity and project progress.
- Update Password: Reset user passwords.

### Approval Team Module

- Approve Bidding: Grant approval to team members based on their bids and provided information.

## Technologies Used

- Front-end: HTML, CSS, JavaScript