<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Groups;
use App\Mail\Pdf;

class ApprovalStatusUpdated extends Mailable
{
    use Queueable, SerializesModels;

    public $group;

    /**
     * Create a new message instance.
     */
    public function __construct(Groups $group)
    {
        $this->group = $group;
    }

    public function build()
    {
        return $this->subject('Project Status Updated')
                    ->view('emails.approval_status_updated');
    }  
}
