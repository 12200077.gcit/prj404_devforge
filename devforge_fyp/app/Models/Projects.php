<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Projects extends Model
{
    use HasFactory, Notifiable;

    protected $primaryKey = 'project_id';
    protected $table = 'projects';
    protected $fillable = [
        'project_id',
        'project_title',
        'prjoect_advisor',
        'project_manager',

        'company_name',
        'web_url',
        'phone',
        'company_email',
        'clo',
        'projectobjectives',

        'students_number',
        'duration',
        'deadline',
        'project_category',
        'status',
    ];

}
