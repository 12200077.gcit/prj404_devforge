<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    use HasFactory;

    protected $primaryKey = 'group_id';
    protected $table = 'groups';
    protected $fillable = [
        'project_id',
        'project_name',
        'team_leader',
        'members',
        'project_status',
        'admin_status',
        'approval_status',
    ];

    protected $casts = [
        'members' => 'array',
    ];
}
