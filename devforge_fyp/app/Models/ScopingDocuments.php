<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScopingDocuments extends Model
{
    use HasFactory;

    protected $table = 'scopingdocuments';
    protected $fillable = [
        'project_id',
        'group_id',
        'scope',
        'deliverables',
        'sitemap',

        'gathering_start',
        'gathering_end',
        'analysis_start',
        'analysis_end',
        'design_start',
        'design_end',
        'coding_start',
        'coding_end',
        'testing_start',
        'testing_end',
        'deployment_start',
        'deployment_end',

        'students_number',
        'duration',
        'deadline',
        'project_category',
        'status',
    ];
}
