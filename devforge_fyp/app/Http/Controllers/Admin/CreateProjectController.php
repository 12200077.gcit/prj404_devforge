<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Projects;
use Carbon\Carbon;

class CreateProjectController extends Controller
{
    public function createProject(Request $request) {
        
        $projects = new Projects();
        $deadline = Carbon::createFromFormat('d-m-Y', $request->deadline)->format('Y-m-d');

        $projects->project_title = $request->project_title;
        $projects->prjoect_advisor = $request->prjoect_advisor;
        $projects->project_manager = $request->project_manager;

        $projects->company_name = $request->company_name;
        $projects->web_url = $request->web_url;
        $projects->phone = $request->phone;
        $projects->company_email = $request->company_email;
        $projects->clo = $request->clo;
        $projects->projectobjectives = $request->projectobjectives;

        $projects->students_number = $request->students_number;
        $projects->duration = $request->duration;

        $deadline = \DateTime::createFromFormat('d-m-Y', $request->deadline);
        $projects->deadline = $deadline->format('Y-m-d');
        $projects->project_category = $request->project_category;
        $projects->status = 'pending';

        $projects->save();

        return redirect()->route('admin.dashboard')->with('success', 'Project successfully added.');
    }
}
