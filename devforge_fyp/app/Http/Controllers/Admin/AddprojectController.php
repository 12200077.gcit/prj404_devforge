<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddprojectController extends Controller
{
    public function addProject(){
        return view("admin.addProject");
    }
}
