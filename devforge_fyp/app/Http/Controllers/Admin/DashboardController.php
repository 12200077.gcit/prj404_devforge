<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Groups;
use App\Models\User;
use App\Models\Projects;
use App\Models\ScopingDocuments;
use Dompdf\Dompdf;
use Dompdf\Options;
use Barryvdh\DomPDF\Facade\Pdf;

class DashboardController extends Controller
{
    public function dashboard() {
        $groups = Groups::all();
        $users = User::all();
        $projects = Projects::all();
    
        return view("admin.dashboard", compact('groups', 'users', 'projects'));
    }

    public function overview() {
        return view("overview");
    }

    public function ongoing() {
        return view("ongoing");
    }

    public function close() {
        $groups = Groups::all();
        $users = User::all();
        $projects = Projects::all();
    
        return view("close", compact('groups', 'users', 'projects'));
    }

     // Download Scoping Documents
  
     public function downloadScopingDocuments($scoping_id){
        $scopingdocuments = Scopingdocuments::find($scoping_id);
    
        if ($scopingdocuments) {
            $project = Projects::find($scopingdocuments->project_id);
            $projectTitle = $project->project_title;
    
            $group = Groups::find($scopingdocuments->group_id);
            $rollNo = $group->team_leader;
    
            $user = User::where('enrollment_number', $rollNo)->first();
            $leaderName = $user->name;
    
            $scopingdocuments = scopingDocuments::where('id', $scoping_id)
                ->select('scope', 'deliverables', 'requirements', 'sitemap')
                ->first();
    
            // Check if the scoping document is available
            if (!$scopingdocuments) {
                dd('Scoping document not found.'); // Debugging statement
                return redirect()->back()->with('error', 'Scoping document not found.');
            }
    
            $data = [
                'title' => $projectTitle,
                'enrollment' => $rollNo,
                'leaderName' => $leaderName,
                'scopingdocuments' => $scopingdocuments
            ];
    
            $pdf = PDF::loadView('pdf.pdfGenerator', $data);
            return $pdf->download('Dev_Forge.pdf');
        } else {
            dd('Scoping document not found.'); // Debugging statement
            return redirect()->back()->with('error', 'Scoping document not found.');
        }
    }
    
}
