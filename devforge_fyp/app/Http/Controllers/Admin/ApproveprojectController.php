<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Groups;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProjectStatusUpdated;


class ApproveprojectController extends Controller
{
    public function approve(){   
        $groups = Groups::all();
        
        return view("admin.approveProject",['groups' => $groups]);
    }
    
    public function approveProject(Request $request) {
        $group = Groups::find($request->group_id);
    
        if ($group) {
            $group->admin_status = $request->status;

            if ($request->status == 'Reject') {
                $group->project_status = 'pending';
            } elseif ($request->status == 'Approved') {
                $group->project_status = 'ongoing';
            }
            
            $group->save();
    
            // Retrieve the team leader's email
            $teamLeaderEnrollment = $group->team_leader; // Assuming this is the enrollment number of the team leader
            $teamLeader = User::where('enrollment_number', $teamLeaderEnrollment)->first();
    
            if ($teamLeader) {
                // dd($teamLeader->email);
    
                Mail::to($teamLeader->email)->send(new ProjectStatusUpdated($group));
            } else {
                return redirect()->route('approve')->with('error', 'Team leader not found.');
            }
    
            return redirect()->route('approve')->with('success', 'Project status updated successfully.');
        } else {
            return redirect()->route('approve')->with('error', 'Group not found.');
        }
    }
    
    
}
