<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Groups;
use App\Models\Projects;
use App\Models\ScopingDocuments;
use Carbon\Carbon;

class ProjectScopingFormController extends Controller
{
    public function projectScopingForm($group_id){
        $project_id = Groups::where('group_id', $group_id)->value('project_id');

        $project = Projects::find($project_id);
    
        if ($project) {
            return view("user.projectScopingForm", compact('project', 'group_id'));
        } else {
            return redirect()->back()->with('error', 'Project not found.');
        }
    }

    public function postScopingForm($group_id, Request $request){
        $project_id = Groups::where('group_id', $group_id)->value('project_id');

        $scopingdocuments = new ScopingDocuments();

        $groups = Groups::where('group_id', $group_id)->first();
        $groups->scoping_status = 'Yes';

        $scopingdocuments->project_id = $project_id;
        $scopingdocuments->group_id = $group_id;
        $scopingdocuments->scope = $request->scope;
        $scopingdocuments->deliverables = $request->deliverables;
        $scopingdocuments->requirements = $request->requirements;
        $scopingdocuments->sitemap = $request->sitemap;
        $scopingdocuments->scoping_status = 'Yes';


        $scopingdocuments->gathering_start = $request->gathering_start;
        $scopingdocuments->gathering_end = $request->gathering_end;
        $scopingdocuments->analysis_start = $request->analysis_start;
        $scopingdocuments->analysis_end = $request->analysis_end;
        $scopingdocuments->design_start = $request->design_start;
        $scopingdocuments->design_end = $request->design_end;
        $scopingdocuments->coding_start = $request->coding_start;
        $scopingdocuments->coding_end = $request->coding_end;
        // $deadline = \DateTime::createFromFormat('d-m-Y', $request->deadline);
        // $scopingdocuments->deadline = $deadline->format('Y-m-d');
        $scopingdocuments->testing_start = $request->testing_start;
        $scopingdocuments->testing_end = $request->testing_end;
        $scopingdocuments->deployment_start = $request->deployment_start;
        $scopingdocuments->deployment_end = $request->deployment_end;

        $scopingdocuments->save();

        $groups->scoping_id = $scopingdocuments->id;
        $groups->save();

        return redirect()->route('myProject')->with('success', 'Scoping Documents successfully send.');
    }
}
