<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function profileComplete(){
        return view("user.profileComplete");
    }

    public function userProfileComplete(Request $request){
    $userId = auth()->user()->id;
    $user = User::find($userId);

    $request->validate([
        'contact_number' => 'required',
        'enrollment_number' => 'required',
        // 'linkedin' => 'required',
        // 'online' => 'required',
        'skills' => 'required|array',
        'interest' => 'required',

    ], [
        'contact_number.required' => 'Contact number is required.',
        'enrollment_number.required' => 'Enrollment number is required.',
        // 'linkedin.required' => 'LinkedIn is required.',
        // 'online.required' => 'Online status is required.',
        'skills.required' => 'Skills are required.',
        'interest.required' => 'Interest is required.',
    ]);

    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->storeAs('images', $imageName, 'public');
        $user->image = $imageName;
    }

    $user->contact_number = $request->input('contact_number');
    $user->enrollment_number = $request->input('enrollment_number');
    $user->linkedin = $request->input('linkedin');
    $user->online = $request->input('online');
    // Convert array of skills to JSON before saving
    $user->skills = json_encode($request->input('skills'));
    $user->interest = $request->input('interest');

    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->storeAs('images', $imageName, 'public');
        $user->image = $imageName;
    }

    $user->save();

    return redirect()->route('home')->with('success', 'Profile updated successfully.');
    }

    public function editProfile($id){
        $users = User::where('id', $id)->first();

        return view('user.profileComplete', ['user' => $users]);
    }

    public function updateProfile(Request $request, $id){
        $user = User::find($id);

        if (!$user) {
            return redirect()->back()->with('error', 'User not found.');
        }

        // Validate form data
        $request->validate([
            'contact_number' => 'required',
            'enrollment_number' => 'required',
            'linkedin' => 'required',
            'online' => 'required',
            'skills' => 'required|array',
            'interest' => 'required',
    
        ], [
            'contact_number.required' => 'Contact number is required.',
            'enrollment_number.required' => 'Enrollment number is required.',
            'linkedin.required' => 'LinkedIn is required.',
            'online.required' => 'Online status is required.',
            'skills.required' => 'Skills are required.',
            'interest.required' => 'Interest is required.',
        ]);

        if ($request->hasFile('image')) {
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $user->image = $imageName;
        }

        $user->contact_number = $request->input('contact_number');
        $user->enrollment_number = $request->input('enrollment_number');
        $user->linkedin = $request->input('linkedin');
        $user->online = $request->input('online');
        // Convert array of skills to JSON before saving
        $user->skills = json_encode($request->input('skills'));
        $user->interest = $request->input('interest');

        $user->save();

        return redirect()->route('home')->with('success', 'User updated successfully.');
    }


}
