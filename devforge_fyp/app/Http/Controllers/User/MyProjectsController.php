<?php

namespace App\Http\Controllers\User;
use App\Models\Projects;
use App\Models\Groups;
use App\Models\ScopingDocuments;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth; 
use Dompdf\Dompdf;
use Dompdf\Options;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MyProjectsController extends Controller
{
    public function myProject(){
        $currentUser = Auth::user();
    
        $groups = Groups::where('team_leader', $currentUser->enrollment_number)
                        ->orWhereJsonContains('members', $currentUser->enrollment_number)
                        ->get();
    
        return view("user.MyProjects", ['groups' => $groups]);
    }

    public function applyProject($id){
        $projects = Projects::find($id);

        return view('user.projectApplyForm', ['projects' => $projects]);
    }

    public function applyGroup(Request $request) {
        
        $groups = new Groups();

        $groups->project_id = $request->project_id;
        $groups->project_name = $request->project_name;
        $groups->team_leader = $request->team_leader;
        $groups->members = $request->members;
        $groups->applied = now();

        $groups->save();

        return redirect()->route('home')->with('success', 'Project successfully Applied.');
    }

    // Download Export Documents
    public function userExports($scoping_id){
        $scopingdocuments = Scopingdocuments::find($scoping_id);
    
        if ($scopingdocuments) {
            $project = Projects::find($scopingdocuments->project_id);
            $projectTitle = $project->project_title;
    
            $group = Groups::find($scopingdocuments->group_id);
            $rollNo = $group->team_leader;
    
            $user = User::where('enrollment_number', $rollNo)->first();
            $leaderName = $user->name;
    
            $scopingdocuments = scopingDocuments::where('id', $scoping_id)
                ->select('scope', 'deliverables', 'requirements', 'sitemap')
                ->first();
    
            if (!$scopingdocuments) {
                return redirect()->back()->with('error', 'Scoping document not found.');
            }
    
            $data = [
                'title' => $projectTitle,
                'enrollment' => $rollNo,
                'leaderName' => $leaderName,
                'scopingdocuments' => $scopingdocuments
            ];
    
            $pdf = PDF::loadView('pdf.pdfGenerator', $data);
            return $pdf->download('Dev_Forge.pdf');
        } else {
            return redirect()->back()->with('error', 'Scoping document not found.');
        }
    }
}
