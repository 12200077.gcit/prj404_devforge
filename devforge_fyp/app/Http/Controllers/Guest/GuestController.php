<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Projects;
use App\Models\Feedbacks;
use Illuminate\Support\Facades\Mail;

class GuestController extends Controller
{
    // Home Page
    public function home(){
        $projects = Projects::where('status', 'pending')->get(); 
        return view("guest.home", compact('projects'));
    }

    // About us
    public function aboutUs(){
        return view("guest.aboutUs");
    }

    // Contact us
    public function contactUs(){
        return view("guest.contactus");
    }

    // Completed projects
    public function completedProjects() {
        $projects = Projects::where('status', 'completed')->get();
    
        return view('guest.completedProjects', ['projects' => $projects]);
    }
    

    public function userFeedback(Request $request){
        $feedbacks = new Feedbacks();

        $feedbacks->sender_name = $request->name;
        $feedbacks->sender_email = $request->email;
        $feedbacks->message = $request->message;

        $feedbacks->save();

        return redirect()->route('contactus')->with('success', 'Feedback successfully sended.');
    }

}
