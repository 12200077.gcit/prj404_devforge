<?php

namespace App\Http\Controllers\ApprovalTeam;
use App\Models\Groups;
use App\Models\User;
use App\Models\Projects;
use App\Models\ScopingDocuments;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApprovalStatusUpdated;
use Dompdf\Dompdf;
use Dompdf\Options;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApproveTeamProjectController extends Controller
{
    public function approvePage(){
        $groups = Groups::where('admin_status', 'Approved')
        ->get();
        
        return view("approval-team.approveProject",['groups' => $groups]);
    }

    // public function approvalProject(Request $request) {
    //     $group = Groups::find($request->group_id);
    
    //     if ($group) {
    //         $group->approval_status = $request->status;

    //         $project_id = $group->project_id;
    //         $project = Projects::find($project_id);

    //         if ($request->status == 'Reject') {
    //             $group->project_status = 'pending';
    //         } elseif ($request->status == 'Approved') {
    //             $group->project_status = 'ongoing';
            
    //             $project_id = $group->project_id;
    //             $project = Projects::find($project_id);
            
    //             if ($project) {
    //                 $project->status = 'ongoing';
    //                 $project->save();
    //             } else {
    //                 return redirect()->back()->with('error', 'Project not found.');
    //             }
    //         }
            
    //         $group->save();
    
    //         $teamLeaderEnrollment = $group->team_leader;
    //         $teamLeader = User::where('enrollment_number', $teamLeaderEnrollment)->first();
    
    //         if ($teamLeader) {
    //             Mail::to($teamLeader->email)->send(new ApprovalStatusUpdated($group));
    //         } else {
    //             return redirect()->route('approvalPage')->with('error', 'Team leader not found.');
    //         }
    
    //         return redirect()->route('approvalPage')->with('success', 'Project status updated successfully.');
    //     } else {
    //         return redirect()->route('approvalPage')->with('error', 'Group not found.');
    //     }
    // }

    public function approvalProject(Request $request)
    {
        $group = Groups::find($request->group_id);
 
        if ($group) {
            $group->approval_status = $request->status;

            $project_id = $group->project_id;
            $project = Projects::find($project_id);

            if ($request->status == 'Reject') {
                $group->project_status = 'pending';
            } elseif ($request->status == 'Approved') {
                $group->project_status = 'ongoing';

                if ($project) {
                    $project->status = 'ongoing';
                    $project->save();
                } else {
                    return redirect()->back()->with('error', 'Project not found.');
                }
            }
         
            $group->save();

            $teamLeaderEnrollment = $group->team_leader;
            $teamLeader = User::where('enrollment_number', $teamLeaderEnrollment)->first();

            if ($teamLeader) {
                Mail::to($teamLeader->email)->send(new ApprovalStatusUpdated($group));
            } else {
                return redirect()->route('approvalPage')->with('error', 'Team leader not found.');
            }

            return redirect()->route('approvalPage')->with('success', 'Project status updated successfully.');
        } else {
            return redirect()->route('approvalPage')->with('error', 'Group not found.');
        }
    }

    /////complete project
    public function completeProject(Request $request) {
        $group = Groups::find($request->group_id);
    
        if ($group) {
            $group->project_status = $request->status;

            $project_id = $group->project_id;
            $project = Projects::find($project_id);

            if ($request->status == 'pending') {
                $group->project_status = 'pending';
            } elseif ($request->status == 'completed') {
                $group->project_status = 'completed';
            
                $project_id = $group->project_id;
                $project = Projects::find($project_id);
            
                if ($project) {
                    $project->status = 'completed';
                    $project->save();
                } else {
                    return redirect()->back()->with('error', 'Project not found.');
                }
            }
            
            $group->save();
           
            return redirect()->route('approvalPage')->with('success', 'Project status updated successfully.');
        } else {
            return redirect()->route('approvalPage')->with('error', 'Group not found.');
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Download Scoping Documents
  
    public function downloadScopingDocuments($scoping_id){
    $scopingdocuments = Scopingdocuments::find($scoping_id);

    if ($scopingdocuments) {
        $project = Projects::find($scopingdocuments->project_id);
        $projectTitle = $project->project_title;

        $group = Groups::find($scopingdocuments->group_id);
        $rollNo = $group->team_leader;

        $user = User::where('enrollment_number', $rollNo)->first();
        $leaderName = $user->name;

        $scopingdocuments = scopingDocuments::where('id', $scoping_id)
            ->select('scope', 'deliverables', 'requirements', 'sitemap')
            ->first();

        if (!$scopingdocuments) {
            return redirect()->back()->with('error', 'Scoping document not found.');
        }

        $data = [
            'title' => $projectTitle,
            'enrollment' => $rollNo,
            'leaderName' => $leaderName,
            'scopingdocuments' => $scopingdocuments
        ];

        $pdf = PDF::loadView('pdf.pdfGenerator', $data);
        return $pdf->download('Dev_Forge.pdf');
    } else {
        return redirect()->back()->with('error', 'Scoping document not found.');
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
// send feedback mail
public function sendFeedback($email, Request $request)
{
    // Validate the request data
    $request->validate([
        'feedback_message' => 'required|string',
        'recipient_email' => 'required|email',
    ]);

    // Retrieve the feedback message and recipient email from the request
    $feedbackMessage = $request->input('feedback_message');
    $recipientEmail = $request->input('recipient_email');

    // Send the feedback email
    Mail::raw($feedbackMessage, function ($message) use ($recipientEmail) {
        $message->to($recipientEmail)
            ->subject('Feedback');
    });

    // Optionally, you can redirect the user back with a success message
    return redirect()->back()->with('success', 'Feedback sent successfully.');
}

    

}
