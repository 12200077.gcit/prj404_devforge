<?php

namespace App\Http\Controllers\ApprovalTeam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Projects;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewSupervisorCreated;

class AddsupervisorController extends Controller
{
    public function addSupervisor(){
        $users = User::where('role', 'admin')->get();

        return view("approval-team.addSupervisor",['users' => $users]);
    }

    // public function createSupervisor(Request $request) {

    //     $existingUser = User::where('email', $request->email)->first();

    //     if ($existingUser) {
    //         // If email already exists, return with an error message
    //         return redirect()->route('addSupervisor')->with('error', 'User with this email already exists.');
    //     }

    //     $users = new User();

    //     $users->name = $request->name;
    //     $users->email = $request->email;
    //     $users->role = 'admin';
    //     $users->password = Hash::make($request->password);

    //     $users->save();

    //     return redirect()->route('addSupervisor')->with('success', 'Supervisor successfully added.');
    // }
    public function createSupervisor(Request $request) {
        $existingUser = User::where('email', $request->email)->first();
    
        if ($existingUser) {
            // If email already exists, return with an error message
            return redirect()->route('addSupervisor')->with('error', 'User with this email already exists.');
        }
    
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'admin';
        $user->password = Hash::make($request->password);
        $user->save();
    
        // Send email to the newly created supervisor
        Mail::to($user->email)->send(new NewSupervisorCreated($user, $request->password));
    
        return redirect()->route('addSupervisor')->with('success', 'Supervisor successfully added.');
    }

    public function deleteSupervisor($id){
        $users = User::where('id', $id)->first();

        if ($users) {
            $users->delete();
            return redirect()->back()->with('success', 'Supervisor deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Supervisor Assitant not found.');
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    public function addProject(){
        return view("approval-team.addProject");
    }

    public function approvalAddProject(Request $request) {
        
        $projects = new Projects();
        $deadline = Carbon::createFromFormat('d-m-Y', $request->deadline)->format('Y-m-d');

        $projects->project_title = $request->project_title;
        $projects->prjoect_advisor = $request->prjoect_advisor;
        $projects->project_manager = $request->project_manager;

        $projects->company_name = $request->company_name;
        $projects->web_url = $request->web_url;
        $projects->phone = $request->phone;
        $projects->company_email = $request->company_email;
        $projects->clo = $request->clo;
        $projects->projectobjectives = $request->projectobjectives;

        $projects->students_number = $request->students_number;
        $projects->duration = $request->duration;
        // $projects->deadline = $request->deadline;
        $deadline = \DateTime::createFromFormat('d-m-Y', $request->deadline);
        $projects->deadline = $deadline->format('Y-m-d');
        $projects->project_category = $request->project_category;
        $projects->status = 'pending';

        $projects->save();

        return redirect()->route('approval.dashboard')->with('success', 'Project successfully added.');
    }
}