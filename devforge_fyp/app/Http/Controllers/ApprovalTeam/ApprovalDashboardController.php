<?php

namespace App\Http\Controllers\ApprovalTeam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApprovalDashboardController extends Controller
{
    public function dashboard(){
        return view("approval-team.dashboard");
    }
}
