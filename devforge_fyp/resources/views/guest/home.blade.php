@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/dashboard/dashboard.css') }}">
@endsection

<x-guest-layout>
    <x-slot name="title">Home Page</x-slot>
    <div class="w-100 herobanner">
        <div class="container-fluid px-md-5 px-3 d-flex align-items-center h-100">
            <div class="col-md-5">
                <h2 class="fw-bold">What is it for?</h2>
                <p style="font-size: 14px;">
                    Welcome to GCIT Devforge, the ultimate platform for student project bidding and collaboration! Here, aspiring developers and designers connect with exciting opportunities to showcase their talents. Browse projects, submit proposals, and let your creativity shine. Our streamlined process ensures the best proposals are chosen, fostering excellence and innovation. Join GCIT Devforge today to embark on a journey of discovery, collaboration, and professional growth!
                </p>
            </div>
        </div>
    </div>

   {{--Available Project Section  --}}
   <div class="container-fluid px-md-5 px-3 mt-5">
    <h3 class="fw-bold">Available Projects</h3>
    <div class="d-flex gap-2 mt-3">
        <button class="sorting-btn active-sorting-btn" data-category="all">All</button>
        <button class="sorting-btn" data-category="Websites">Website</button>
        <button class="sorting-btn" data-category="App">Mobile App</button>
    </div>
    
    <div class="row my-3 g-3" id="projectCards">
        @foreach($projects as $project)
        <div class="col-md-3 project-card" data-category="{{ $project->project_category }}">
            <div class="project-card border rounded-5 border-dark-subtle">
                <div class="d-flex justify-content-end pe-4">
                    <div style="font-size: 10px;width:85px;height:38px;border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;" class="bg-secondary text-white text-center p-1">
                        <span>Closing Soon {{ $project->deadline }}</span>
                    </div>
                </div>
                <h6 class="px-3 pt-0 mt-0 fw-bold">{{ $project->project_title }}</h6>
                <p style="font-size: 12px;height:100px;" class="px-3 overflow-y-scroll">
                    {{ $project->projectobjectives }}
                </p>
                <div class="d-flex flex-wrap gap-1 px-3">
                    <div style="font-size: 12px;color:#1a1919e4;height:23px;" class="border rounded-5 border-dark-subtle fw-bold d-flex justify-content-center align-items-center gap-2 px-2">
                        <span>
                            <box-icon size='xs' name='user' color='#6d6c6c'></box-icon>
                        </span>
                        <span>{{ $project->students_number }} Students</span>
                    </div>
                    <div style="font-size: 12px;color:#1a1919e4;height:23px;" class="border rounded-5 border-dark-subtle fw-bold d-flex justify-content-center align-items-center gap-2 px-2">
                        <span>
                            <box-icon size='xs' name='calendar' color='#6d6c6c'></box-icon>
                        </span>
                        <span>{{ $project->duration }} Months</span>
                    </div>
                    <div style="font-size: 12px;color:#1a1919e4;height:23px;" class="border rounded-5 border-dark-subtle fw-bold d-flex justify-content-center align-items-center px-2 project-category">{{ $project->project_category }}</div>
                </div>
                <div class="px-3 mt-xl-4 mt-md-2 mt-3 {{ (Auth::check() && (Auth::user()->role === 'admin' || Auth::user()->role === 'approvalteam')) ? 'd-none' : '' }}">
                    <a href="{{ url('/applyProject', $project->project_id) }}" class="btn-primary login-register-btn btn w-100 rounded-4 text-white">Apply Now</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const sortingButtons = document.querySelectorAll('.sorting-btn');
            const projectCards = document.querySelectorAll('.project-card');

            sortingButtons.forEach(button => {
                button.addEventListener('click', function() {
                    const category = this.getAttribute('data-category');
                    console.log(`Button clicked: ${category}`);

                    // Remove 'active-sorting-btn' class from all buttons
                    sortingButtons.forEach(btn => btn.classList.remove('active-sorting-btn'));
                    // Add 'active-sorting-btn' class to the clicked button
                    this.classList.add('active-sorting-btn');

                    // Filter project cards based on the category
                    projectCards.forEach(card => {
                        const cardCategory = card.querySelector('.project-category').innerText.trim();
                        console.log(`Card category: ${cardCategory}`);
                        if (category === 'all' || cardCategory === category) {
                            card.style.display = 'block';
                        } else {
                            card.style.display = 'none';
                        }
                    });
                });
            });
        });
    </script>
</x-guest-layout>
