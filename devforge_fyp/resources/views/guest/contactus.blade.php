<x-guest-layout>
    <x-slot name="title">Contact Us</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <div class="row mt-5">
            <div class="col-md-7">
                <h2 class="fw-bold">Talk to us</h2>
                <p>We're here to help and eager to hear from you! Whether you have questions, feedback, or inquiries, don't hesitate to reach out. Our team is committed to providing timely and helpful assistance. We look forward to connecting with you!</p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-3">
                <form method="POST" action="{{ route('userFeedback') }}">
                    @csrf
                    <input required class="form-control mb-2 rounded-5 py-2" type="text" name="name" placeholder="Name" name="name">
                    <input required class="form-control mb-2 rounded-5 py-2" type="email" name="email" placeholder="Email" name="email">
                    <textarea required class="form-control mb-2 rounded-4 pt-3" name="message" name="message" rows="4" placeholder="message"></textarea>
                    <button class="form-control mt-3 btn btn-primary text-white rounded-5" type="submit">Send</button>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>