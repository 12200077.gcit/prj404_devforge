<x-guest-layout>
    <x-slot name="title">Completed Projects</x-slot>

    {{--Available Project Section  --}}
   <div class="container-fluid px-md-5 px-3 mt-3">
    <h3 class="fw-bold">Completed Projects</h3>
    {{-- sorting buttons --}}
    <div class="d-flex gap-2 mt-3">
       <button class="sorting-btn active-sorting-btn">All</button>
       <button class="sorting-btn">Website</button>
       <button class="sorting-btn">Mobile App</button>
    </div>
    
    <div class="row my-3 g-3">
        @foreach($projects as $project)
        <div class="col-md-3">
            <div class="project-card border rounded-5 border-dark-subtle">
                <div class="d-flex justify-content-end pe-4">
                    <div style="font-size: 10px;width:85px;height:38px;border-bottom-left-radius: 16px;border-bottom-right-radius: 16px;" class="bg-secondary text-white text-center p-1">
                        <span>closing soon {{ $project->deadline }}</span>
                    </div>
                </div>
                <h6 class="px-3 pt-0 mt-0 fw-bold">{{ $project->project_title }}</h6>
                <p style="font-size: 12px;height:100px;" class="px-3 overflow-y-scroll">
                    {{ $project->projectobjectives }}
                </p>
                <div class="d-flex flex-wrap gap-1 px-3">
                    <div style="font-size: 12px;color:#1a1919e4;height:23px;" class="border rounded-5 border-dark-subtle fw-bold d-flex justify-content-center align-items-center gap-2 px-2">
                        <span>
                            <box-icon size='xs' name='user' color='#6d6c6c'></box-icon>
                        </span>
                        <span>{{ $project->students_number }} Students</span>
                    </div>
                    <div style="font-size: 12px;color:#1a1919e4;height:23px;" class="border rounded-5 border-dark-subtle fw-bold d-flex justify-content-center align-items-center gap-2 px-2">
                        <span>
                            <box-icon size='xs' name='calendar' color='#6d6c6c'></box-icon>
                        </span>
                        <span>{{ $project->duration }} Months</span>
                    </div>
                    <div style="font-size: 12px;color:#1a1919e4;height:23px;" class="border rounded-5 border-dark-subtle fw-bold d-flex justify-content-center align-items-center px-2">{{ $project->project_category }}</div>
                </div>
                {{-- <div class="px-3 mt-xl-4 mt-md-2 mt-3">
                    <a href="{{ url('/applyProject', $project->project_id) }}" class="btn-primary login-register-btn btn w-100 rounded-4 text-white">Apply Now</a>
                </div> --}}
            </div>
        </div>
        @endforeach
    </div>

  </div>
</x-guest-layout>