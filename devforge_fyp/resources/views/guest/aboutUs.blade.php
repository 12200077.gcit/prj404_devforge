<x-guest-layout>
    <x-slot name="title">About Us</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <div class="row mt-5">
            <div class="col-md-8">
                <h2 class="fw-bold">What DevForge is?</h2>
                <p>
                    Welcome to GCIT Devforge, a student-focused platform for project bidding and collaboration!
                     Connect with exciting opportunities, showcase your creativity, and compete for assignments. 
                     Our streamlined selection process ensures top proposals are chosen. 
                    Join today to embark on a journey of discovery, collaboration, and professional growth!
                </p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-8">
                <h2 class="fw-bold">Current Students in DevForge</h2>
                <p>
                    Meet the talented students currently engaged in transformative projects within DevForge. These dedicated individuals represent the diverse skills and innovative spirit driving the success of DevForge projects
                </p>
            </div>
        </div>
        <div class="row g-2">
            @php
                $count = 6; 
            @endphp
            
            @foreach (range(1, $count) as $index)
                <div class="col-md-2">
                    <div style="height: 160px;" class="bg-primary-subtle rounded-4">
                        <img class="rounded-4" style="width: 100%;height:100%;object-fit:cover;" src="https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="">
                    </div>
                </div>  
            @endforeach
        </div>
        <div class="row mt-5">
            <div class="col-md-8">
                <h2 class="fw-bold">Graduates from DevForge</h2>
                <p>
                    Congratulations to the talented graduates who have honed their skills and launched successful careers through DevForge:
                    These exceptional individuals are a testament to the impact and effectiveness of the DevForge platform in preparing students for the workforce and beyond
                </p>
            </div>
        </div>
        <div class="row g-2 mb-3">
            @php
                $count = 12; 
            @endphp
            
            @foreach (range(1, $count) as $index)
                <div class="col-md-2">
                    <div style="height: 160px;" class="bg-primary-subtle rounded-4">
                        <img class="rounded-4" style="width: 100%;height:100%;object-fit:cover;" src="https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="">
                    </div>
                </div>  
            @endforeach
        </div>
    </div>

</x-guest-layout>