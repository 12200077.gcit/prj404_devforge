@props(['messages'])

@if ($messages)
    <p style="list-style: none;" {{ $attributes->merge(['class' => 'text-danger']) }}>
        @foreach ((array) $messages as $message)
            <span>{{ $message }}</span>
        @endforeach
    </p>
@endif
