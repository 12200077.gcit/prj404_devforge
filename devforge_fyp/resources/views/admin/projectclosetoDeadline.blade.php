<div class="container">
    <div style="height: 60vh;" class="overflow-y-scroll">
        <table class="table">
            <thead>
              <tr>
                <th>Project Name</th>
                <th>Student Name</th>
                <th>Enrollment Number</th>
                <th>Supervisor</th>
                <th>Deadline</th>
                <th>Task</th>
                <th>Scoping Document</th>
              </tr>
            </thead>
            <tbody>
                @foreach(\App\Models\Groups::where('admin_status', 'Approved')->where('approval_status', 'Approved')->get() as $group)
                    @php
                        $teamLeader = \App\Models\User::where('enrollment_number', $group->team_leader)->first();
                        $projectAdvisorName = \App\Models\Projects::where('project_id', $group->project_id)->value('prjoect_advisor');
                        $deadline = \App\Models\Projects::where('project_id', $group->project_id)->value('deadline');
                        
                        $currentDate = now()->toDateString();
                        $sd = \App\Models\ScopingDocuments::where('id', $group->scoping_id)->first();
                        $task = '';

                        if ($currentDate >= $sd->gathering_start && $currentDate <= $sd->gathering_end) {
                            $task = 'gathering';
                        } elseif ($currentDate >= $sd->analysis_start && $currentDate <= $sd->analysis_end) {
                            $task = 'analysis';
                        } elseif ($currentDate >= $sd->design_start && $currentDate <= $sd->design_end) {
                            $task = 'design';
                        }elseif ($currentDate >= $sd->coding_start && $currentDate <= $sd->coding_end) {
                            $task = 'coding';
                        }elseif ($currentDate >= $sd->testing_start && $currentDate <= $sd->testing_end) {
                            $task = 'testing';
                        }elseif ($currentDate >= $sd->deployment_start && $currentDate <= $sd->deployment_end) {
                            $task = 'deployment';
                        } else{
                          $task = 'Continuing previous task';
                        }
                        // if ($sd !== null) {
                        //     if (
                        //         ($currentDate >= $sd->gathering_start && $currentDate <= $sd->gathering_end) ||
                        //         ($currentDate >= $sd->analysis_start && $currentDate <= $sd->analysis_end) ||
                        //         ($currentDate >= $sd->design_start && $currentDate <= $sd->design_end) ||
                        //         ($currentDate >= $sd->coding_start && $currentDate <= $sd->coding_end) ||
                        //         ($currentDate >= $sd->testing_start && $currentDate <= $sd->testing_end) ||
                        //         ($currentDate >= $sd->deployment_start && $currentDate <= $sd->deployment_end)
                        //     ) {
                        //         if ($currentDate >= $sd->gathering_start && $currentDate <= $sd->gathering_end) {
                        //             $task = 'gathering';
                        //         } elseif ($currentDate >= $sd->analysis_start && $currentDate <= $sd->analysis_end) {
                        //             $task = 'analysis';
                        //         } elseif ($currentDate >= $sd->design_start && $currentDate <= $sd->design_end) {
                        //             $task = 'design';
                        //         } elseif ($currentDate >= $sd->coding_start && $currentDate <= $sd->coding_end) {
                        //             $task = 'coding';
                        //         } elseif ($currentDate >= $sd->testing_start && $currentDate <= $sd->testing_end) {
                        //             $task = 'testing';
                        //         } elseif ($currentDate >= $sd->deployment_start && $currentDate <= $sd->deployment_end) {
                        //             $task = 'deployment';
                        //         }
                        //     } else {
                        //         $task = 'Continuing previous task';
                        //     }
                        // } else {
                        //     // Handle the case when $sd is null
                        // }


                    @endphp
                        <tr>
                            <td class="text-primary">{{ $group->project_name }}</td>
                            <td class="text-primary">{{ $teamLeader->name }}</td>
                            <td class="text-primary">{{ $teamLeader->enrollment_number }}</td>
                            <td class="text-primary">{{  $projectAdvisorName }}</td>
                            <td class="text-primary">{{  date('F j, Y', strtotime($deadline)) }}</td>
                            <td class="text-primary">{{ $task }}</td>
                            <td class="text-primary">
                                @php
                                    $routeName = Route::currentRouteName();
                                @endphp
                                @if(isset($group->scoping_id))
                                    <a href="{{ $routeName === 'admin.route.name' ? route('downloadAdmin', ['scoping_id' => $group->scoping_id]) : route('downloadScopingDocuments', ['scoping_id' => $group->scoping_id]) }}">
                                        Download
                                    </a>
                                @else
                                   No Scoping Documents
                                @endif
                            </td>
                            
                          </td>
                        </tr>
                  
                @endforeach
            </tbody>
        </table>
    </div>
</div>