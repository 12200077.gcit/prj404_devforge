@section('page-css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
<x-app-layout>
    <x-slot name="title">Add Project</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <h2 class="fw-bold mt-5 mb-3">Add Project</h2>
        {{-- add projcet form --}}
        <form method="POST" id="addProjectForm" action="{{ route('addProject') }}"  enctype="multipart/form-data">
            @csrf
            <div class="row g-5">
                <div class="col-md-4">
                    <div>
                        <h5 class="fw-bold mb-3">Project Overview</h5>
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="project_title" name="project_title" placeholder="Project Title">
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="prjoect_advisor" name="prjoect_advisor" placeholder="Project Advisor">
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="project_manager" name="project_manager" placeholder="Project Manager">
                    </div>
                </div>
                <div class="col-md-4">
                    <div>
                        <h5 class="fw-bold mb-3">Client Details</h5>
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="company_name" name="company_name" placeholder="Company Name">
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="web_url" name="web_url" placeholder="Web URL">
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="phone" name="phone" placeholder="Telephone/Mobile">
                        <input required class="form-control project-input rounded-5 mb-2" type="email" id="company_email" name="company_email" placeholder="Email">
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="clo" name="clo" placeholder="Company Liason Officer (CLO)">
                    </div>
                </div>
                <div class="col-md-4">
                    <div>
                        <h5 class="fw-bold mb-3">Project Overview</h5>
                        <textarea required class="form-control project-input" id="projectobjectives" name="projectobjectives" rows="8" placeholder="Message"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <h5 class="fw-bold mb-3">Project Details</h5>
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="students_number" name="students_number" placeholder="Number of Students">
                        <input required class="form-control project-input rounded-5 mb-2" type="text" id="duration" name="duration" placeholder="Duration">
                        <input id="deadline" required class="form-control project-input rounded-5 mb-2" type="text" id="deadline" name="deadline" placeholder="Deadline">
                        <select required class="form-control form-select rounded-5 mb-2" id="project_category" name="project_category">
                            <option value="">Project Category</option>
                            <option value="App">App</option>
                            <option value="Websites">Website</option>
                        </select>                    
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="h-100 d-flex justify-content-end align-items-end">
                        <div class="d-flex gap-2">
                            <button onclick="clearForm()" type="button" class="btn btn btn-outline-primary px-4 rounded-5"> Clear Form</button>
                            <button type="submit" class="btn btn-primary text-white px-4 rounded-5">Add Project</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        // Initialize Flatpickr on the date input field
        flatpickr("#deadline", {
            dateFormat: "d-m-Y", // Date format (optional)
            minDate: tomorrow,
        });
        function clearForm() {
            // Get all input elements inside the form
            var inputs = document.getElementById("addProjectForm").getElementsByClassName("project-input");
    
            // Loop through each input and set its value to an empty string
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].value = "";
            }
        }
    </script>

</x-app-layout>