@php
    $openProjectsCount = \App\Models\Projects::where('status', 'pending')->count();
    $closedProjectsCount = \App\Models\Projects::where('status', 'completed')->count();
    $inProgressProjectsCount = \App\Models\Projects::where('status', 'ongoing')->count();
@endphp

<div class="container">
    <div class="row g-2">
        {{-- Pie --}}
        <div class="col-md-2 border-end border-dark-subtle">
            <div class=" mt-5 pe-4">
                 <!-- Canvas for the pie chart -->
                 <canvas id="myPieChart" width="100%" height="100"></canvas>
            </div>
            <div id="legend"  class="mt-5"></div>
        </div>

        {{-- Resource Planning --}}
        <div class="col-md-5 border-end border-dark-subtle px-md-5">
            <div>
                <h5 class="fw-bold">Resource Planning</h5>
                <div class="d-flex justify-content-between mt-3">
                    <div>
                        @php
                                use App\Models\Groups;

                                $total = \App\Models\User::where('role', 'user')->count();
                                $leader = \App\Models\Groups::where('project_status', 'ongoing')
                                        ->select('team_leader')
                                        ->distinct()
                                        ->count();

                                        $membersArrays = Groups::where('project_status', 'ongoing')->pluck('members');
                                        $allMembers = [];

                                        foreach ($membersArrays as $membersArray) {
                                            if (is_array($membersArray)) {
                                                $allMembers = array_merge($allMembers, $membersArray);
                                            }
                                        }

                                        $uniqueMembers = array_unique($allMembers);
                                        $uniqueMembersCount = count($uniqueMembers);
                            @endphp
                        <h1 class="text-primary fw-bold text-center">{{ $leader +  $uniqueMembersCount }}</h1>

                        <span class="text-center d-block">Students working <br> on projects</span>
                    </div>
                    <div>
                        <h1 class="text-secondary fw-bold text-center">{{$total - ($leader +  $uniqueMembersCount) }}</h1>
                        <span class="text-center d-block">
                            Students available <br> for projects
                        </span>
                    </div>
                </div>

                <form action="" method="GET">
                    <input type="search" name="search" class="form-control rounded-5 my-3" placeholder="Search name" value="{{ request('search') }}">
                </form>
                
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Student <br> Name</th>
                                <th>Enrollment <br> Number</th>
                                <th>Project in <br> Progress</th>
                                <th>Project <br> Completed</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Models\Groups::all() as $group)
                                @php
                                    $teamLeader = \App\Models\User::where('enrollment_number', $group->team_leader)->first();
                                @endphp
                                @if($teamLeader && (empty(request('search')) || stripos($teamLeader->name, request('search')) !== false))
                                    <tr>
                                        <td class="text-primary">{{ $teamLeader->name }}</td>
                                        <td class="text-primary">{{ $group->team_leader }}</td>
                                        <td class="text-primary">
                                            @php
                                                $ongoingProjects = \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                                            ->where('project_status', 'ongoing')
                                                                                            ->count();
                                            @endphp
                                            {{ $ongoingProjects }}
                                        </td>
                                        <td class="text-primary">
                                            @php
                                                $completedProjectsCount = \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                                            ->where('project_status', 'completed')
                                                                                            ->count();
                                            @endphp
                                            {{ $completedProjectsCount }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                
                    <div class="w-100 d-flex justify-content-center">
                        <button class="btn text-primary">View More</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- List of Projects close to deadline --}}
        <div class="col-md-5">
            <div class="px-md-5">
                <h5 class="fw-bold mt-5">List of Projects close to deadline</h5>
                <div class="table-responsive">
                    <table class="table table-borderless mt-3">
                        <thead>
                          <tr>
                            <th>No. of days left</th>
                            <th>Project Name</th>
                            <th>Deadline</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Models\Projects::all() as $project)
                            <tr>
                                <td class="text-center">{{ round(abs(\Carbon\Carbon::now()->diffInDays($project->deadline))) }}</td>
                                <td class="text-center overflow-x-scroll" style="max-width: 250px;">{{ $project->project_title }}</td>
                                <td class="text-center">{{ \Carbon\Carbon::parse($project->deadline)->format('F d, Y') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="w-100 d-flex justify-content-center">
                    <button class="btn text-primary">View More</button>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Pie chart js --}}
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    var ctx = document.getElementById('myPieChart').getContext('2d');
    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['Open Projects', 'Closed Projects', 'In-progress'],
            datasets: [{
                data: [
                    {{ $openProjectsCount }},   
                    {{ $closedProjectsCount }}, 
                    {{ $inProgressProjectsCount }},
                ],
                backgroundColor: ['#9747FF', '#74B444', '#F48422'],
            }]
        },
        options: {
            cutoutPercentage: 10,
            legend: {
                display: false
            },
            plugins: {
                legend: {
                    display: false
                },
                tooltip: {
                    enabled: false
                }
            },
            elements: {
                arc: {
                    borderWidth: 0
                }
            }
        }
    });
    // Custom legend
    var legend = document.getElementById('legend');
    var labels = myPieChart.data.labels;
    var backgroundColors = myPieChart.data.datasets[0].backgroundColor;
    var values = myPieChart.data.datasets[0].data;
    for (var i = 0; i < labels.length; i++) {
        var label = document.createElement('div');
        label.innerHTML = '<span style="display:inline-block;width:12px;height:12px;border-radius:100px;background-color:' + backgroundColors[i] + '"></span> ' + labels[i] + ': ' + values[i];
        legend.appendChild(label);
    }
</script>



