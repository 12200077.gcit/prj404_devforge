@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/approveprojects.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">Approve Projects</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <div class="row">
            <h2 class="mt-4 fw-bold">Projects for Approval (Supervisor)</h2>
            <div class="table-responsive mt-3">
                <table class="table">
                    <thead>
                      <tr>
                        <th>Project Name</th>
                        <th>Team Leader</th>
                        <th>Members</th>
                        <th>Status</th>
                      </tr>
                    </thead>

                    <tbody>
                        @foreach($groups as $group)
                        <tr>
                            <td class="py-3">{{ $group->project_name }}</td>
                            @php
                                $teamLeader = \App\Models\User::where('enrollment_number', $group->team_leader)->first();
                            @endphp
                            <td class="text-primary">
                                <a class="text-decoration-none" href="#" data-bs-toggle="modal"  data-bs-target="#studentDetails" data-teamleader="{{ json_encode($teamLeader) }}">{{ $group->team_leader }}</a>
                            </td>
                            <td class="text-primary">
                                @php
                                    $membersArray = is_string($group->members) ? json_decode($group->members, true) : $group->members;
                                @endphp
                            
                                @foreach($membersArray as $memberEnrollmentNumber)
                                    @php
                                        $member = \App\Models\User::where('enrollment_number', $memberEnrollmentNumber)->first();
                                    @endphp
                                    @if($member)
                                        <a class="text-decoration-none" href="#" data-bs-toggle="modal" data-bs-target="#studentDetails" data-teamleader="{{ json_encode($member) }}">{{ $member->enrollment_number }}</a>@if(!$loop->last),@endif
                                    @else
                                        {{ $memberEnrollmentNumber }}@if(!$loop->last),@endif
                                    @endif
                                @endforeach
                            </td>
                            
                            <td class="py-3 d-flex gap-2">
                                @if($group->admin_status == 'pending')
                                    {{-- Reject Form --}}
                                    <form method="POST" action="{{ route('approveProject') }}">
                                        @csrf
                                        <input type="hidden" name="group_id" value="{{ $group->group_id }}">                                    
                                        <input type="hidden" name="status" value="Reject">
                                        <button style="height: 30px;" class="btn btn-secondary text-white rounded-5 d-flex justify-content-center align-items-center px-3" type="submit">Reject</button>
                                    </form>
                                    {{-- Approve Form --}}
                                    <form method="POST" action="{{ route('approveProject') }}">
                                        @csrf
                                        <input type="hidden" name="group_id" value="{{ $group->group_id }}">                                    
                                        <input type="hidden" name="status" value="Approved">
                                        <button style="height: 30px;" class="btn btn-primary text-white rounded-5 d-flex justify-content-center align-items-center px-3" type="submit">Approve</button>
                                    </form>
                                @else
                                    @if($group->admin_status == 'Approved')
                                        <span class="text-success">Approved by Admin</span>
                                    @else
                                        <span class="text-danger">Rejected by Admin</span>
                                    @endif
                                @endif
                            </td>                            
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

  <!-- Modal profile details -->
  <div class="modal fade" id="studentDetails" tabindex="-1" aria-labelledby="studentDetailsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content ms-5 border-0 rounded-4" style="box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;">
            <div class="modal-body px-4 pb-4">
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="d-flex align-items-center gap-2">
                    <div>
                        <img hidden id="teamLeaderImage" style="width: 80px; height: 80px; " class="rounded-circle" src="#" alt="">
                    </div>
                    <div>
                        <h6 style="font-size:14px;" class="mt-3 text-primary" id="leaderName"></h6>
                        <p style="width: 80%;font-size:12px;" id="leaderDescription"></p>
                    </div>
                    <div class="text-primary" style="font-size: 13px;">
                        <div class="ms-auto" id="skills"></div>
                    </div>
                </div>
                <div class="border border-dark-subtle border-opacity-10 w-100 rounded-5 py-2 px-4 mt-3">
                    <div class="d-flex" style="font-size: 13px;">
                        <span class="fw-bold">Active Projects</span>
                            @php
                                $ongoingProjectsCount = 0;
                                    if (!empty($group)) {
                                        $ongoingProjectsCount = \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                                ->where('project_status', 'ongoing')
                                                                                ->count();
                                    }
                            @endphp
                        <span class="text-primary ms-auto">
                            {{$ongoingProjectsCount}}
                        </span>
                    </div>
                    <div class="d-flex mt-2" style="font-size: 13px;">
                        <span class="fw-bold">Completed Projects</span>
                            @php
                            $completedProjectsCount = 0;
                            if (!empty($group)) {
                                $completedProjectsCount = \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                            ->where('project_status', 'completed')
                                                                            ->count();
                            }
                            @endphp
                        <span class="text-primary ms-auto">
                            {{$completedProjectsCount}}
                        </span>
                    </div>
                </div>
                <div class="mt-3">
                    <h6 class="fw-bold" style="font-size: 13px;">More Information</h6>
                    <div class="d-flex flex-column gap-2">
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Enrollment</div>
                            <div class="ms-auto" id="enrollment"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Email</div>
                            <div class="ms-auto" id="email"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Contact No.</div>
                            <div class="ms-auto" id="contact"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Linkedin Profile</div>
                            <div class="ms-auto text-primary" id="linkedin"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Portfolio</div>
                            <div class="ms-auto text-primary" id="portfolio"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function handleLinkClick(event) {
        event.preventDefault();
        var teamLeaderData = JSON.parse(event.target.getAttribute('data-teamleader'));

        var imageUrl = "{{ asset('/storage/images/') }}/" + teamLeaderData.image;
        document.getElementById('teamLeaderImage').src = imageUrl;

        
        document.getElementById('skills').textContent = teamLeaderData.skills;
        document.getElementById('leaderName').textContent = teamLeaderData.name;
        document.getElementById('leaderDescription').textContent = teamLeaderData.description;
        document.getElementById('enrollment').textContent = teamLeaderData.enrollment_number;
        document.getElementById('email').textContent = teamLeaderData.email;
        document.getElementById('contact').textContent = teamLeaderData.contact_number;
        document.getElementById('linkedin').textContent = teamLeaderData.linkedin;
        document.getElementById('portfolio').textContent = teamLeaderData.online;
    }

    document.querySelectorAll('.text-decoration-none').forEach(function(link) {
        link.addEventListener('click', handleLinkClick);
    });
</script>
</x-app-layout>