<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-md-9">
            <div>
               <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="mt-3">
                            @php
                                use App\Models\Groups;

                                $total = \App\Models\User::where('role', 'user')->count();
                                $leader = \App\Models\Groups::where('project_status', 'ongoing')
                                        ->select('team_leader')
                                        ->distinct()
                                        ->count();

                                        $membersArrays = Groups::where('project_status', 'ongoing')->pluck('members');
                                        $allMembers = [];

                                        foreach ($membersArrays as $membersArray) {
                                            if (is_array($membersArray)) {
                                                $allMembers = array_merge($allMembers, $membersArray);
                                            }
                                        }

                                        $uniqueMembers = array_unique($allMembers);
                                        $uniqueMembersCount = count($uniqueMembers);
                            @endphp

                            <h1 class="text-primary fw-bold text-center">{{ ($leader + $uniqueMembersCount) }}</h1>
                            <span class="text-center d-block">Students working <br> on projects</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                            <h5 class="fw-bold">Resource Planning</h5>
                            <form action="" method="GET">
                                <input type="search" name="search" class="form-control rounded-5 my-3" placeholder="Search name" value="{{ request('search') }}">
                            </form>
                            <div style="height: 50vh;" class="overflow-y-scroll table-responsive">

                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th>Student <br> Name</th>
                                        <th>Enrollment <br> Number</th>
                                        <th>Project in <br> Progress</th>
                                        <th>Project <br> Completed</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(\App\Models\Groups::all() as $group)
                                                @php
                                                    $teamLeader = \App\Models\User::where('enrollment_number', $group->team_leader)->first();
                                                @endphp
                                                @if($teamLeader && (empty(request('search')) || stripos($teamLeader->name, request('search')) !== false))
                                                    <tr>
                                                        <td class="text-primary">
                                                            <a class="text-decoration-none" href="#" data-bs-toggle="modal" data-bs-target="#studentDetails" data-teamleader="{{ json_encode($teamLeader) }}">{{ $teamLeader->name }}</a>
                                                        </td>
                                                        <td class="text-primary">{{ $group->team_leader }}</td>
                                                        <td class="text-primary">
                                                            {{ \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                ->where('project_status', 'ongoing')
                                                                ->count() }}
                                                        </td>
                                                        <td class="text-primary">
                                                            {{ \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                ->where('project_status', 'completed')
                                                                ->count() }}
                                                        </td>
                                                    </tr>
                                                @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mt-3">
                            <h1 class="text-secondary fw-bold text-center">{{$total - ($leader + $uniqueMembersCount) }}</h1>
                            <span class="text-center d-block">Students available <br> for projects</span>
                        </div>
                    </div>
                </div>
               </div>
            </div>
        </div>
    </div>
</div>
  <!-- Modal profile details -->
  <div class="modal fade" id="studentDetails" tabindex="-1" aria-labelledby="studentDetailsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content ms-5 border-0 rounded-4" style="box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;">
            <div class="modal-body px-4 pb-4">
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="d-flex align-items-center gap-2">
                    <div>
                        <img id="teamLeaderImage" style="width: 80px;height:80px;object-fit:cover;object-position:center;" class="rounded-circle" src="" alt="">
                    </div>
                    <div>
                        <h6 style="font-size:14px;" class="mt-3 text-primary" id="leaderName"></h6>
                        <p style="width: 80%;font-size:12px;" id="leaderDescription"></p>
                    </div>
                </div>
                <div class="border border-dark-subtle border-opacity-10 w-100 rounded-5 py-2 px-4 mt-3">
                    <div class="d-flex" style="font-size: 13px;">
                        <span class="fw-bold">Active Projects</span>
                            {{-- @php
                                $ongoingProjectsCount = 0;

                                if (isset($groups)) {
                                    $ongoingProjectsCount = \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                                ->where('project_status', 'ongoing')
                                                                                ->count();
                                    }
                            @endphp --}}
                            @php
    $ongoingProjectsCount = 0;

    if (!empty($group)) {
        $ongoingProjectsCount = \App\Models\Groups::where('team_leader', $group->team_leader)
                                                  ->where('project_status', 'ongoing')
                                                  ->count();
    }
@endphp

                        <span class="text-primary ms-auto">
                            {{$ongoingProjectsCount}}
                        </span>
                    </div>
                    <div class="d-flex mt-2" style="font-size: 13px;">
                        <span class="fw-bold">Completed Projects</span>
                        @php
                            $completedProjectsCount = 0;
                            
                            if (isset($groups)) {
                                foreach ($groups as $group) {
                                    $completedProjectsCount += \App\Models\Groups::where('team_leader', $group->team_leader)
                                                                                 ->where('project_status', 'completed')
                                                                                 ->count();
                                }
                            }
                        @endphp
                    
                        <span class="text-primary ms-auto">
                            {{ $completedProjectsCount }}
                        </span>
                    </div>
                </div>
                <div class="mt-3">
                    <h6 class="fw-bold" style="font-size: 13px;">More Information</h6>
                    <div class="d-flex flex-column gap-2">
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Enrollment</div>
                            <div class="ms-auto" id="enrollment"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Email</div>
                            <div class="ms-auto" id="email"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Contact No.</div>
                            <div class="ms-auto" id="contact"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Linkedin Profile</div>
                            <div class="ms-auto text-primary" id="linkedin"></div>
                        </div>
                        <div class="d-flex" style="font-size: 13px;">
                            <div>Portfolio</div>
                            <div class="ms-auto text-primary" id="portfolio"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function handleLinkClick(event) {
        event.preventDefault();
        var teamLeaderData = JSON.parse(event.target.getAttribute('data-teamleader'));

        document.getElementById('teamLeaderImage').src = teamLeaderData.image;
        document.getElementById('leaderName').textContent = teamLeaderData.name;
        document.getElementById('leaderDescription').textContent = teamLeaderData.description;
        document.getElementById('enrollment').textContent = teamLeaderData.enrollment_number;
        document.getElementById('email').textContent = teamLeaderData.email;
        document.getElementById('contact').textContent = teamLeaderData.contact_number;
        document.getElementById('linkedin').textContent = teamLeaderData.linkedin;
        document.getElementById('portfolio').textContent = teamLeaderData.online;
    }

    document.querySelectorAll('.text-decoration-none').forEach(function(link) {
        link.addEventListener('click', handleLinkClick);
    });
</script>
