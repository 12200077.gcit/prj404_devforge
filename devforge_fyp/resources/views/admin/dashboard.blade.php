@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/admindashboard.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">Dashboard</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs mt-5" id="myTab" role="tablist">
            <li class="nav-item">
                <a style="font-size: 18px;font-weight:600;" class="nav-link active text-dark" id="approval-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Overview</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 18px;font-weight:600;" class="nav-link text-dark" id="ongoing-project-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Ongoing Project</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 18px;font-weight:600;" class="nav-link text-dark" id="project-tab-close-deadline" data-bs-toggle="tab" href="#project-close-deadline" role="tab" aria-controls="project-close-deadline" aria-selected="false">Project Close to Deadline</a>
            </li>
        </ul>
         <!-- Tab panes -->
        <div style="min-height: 65vh;" class="tab-content border border-top-0 rounded-bottom-5 border-dark-subtl border-opacity-25 p-5 mb-3">
            {{-- Overview --}}
            <div class="tab-pane fade show active table-responsive" id="home" role="tabpanel" aria-labelledby="approval-tab">
                {{-- @include('admin.overview') --}}
                @include('admin.overview', ['groups' => $groups, 'users' => $users, 'projects' => $projects])
            </div>
            {{-- Ongoing Project --}}
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="ongoing-project-tab">
                @include('admin.ongoingProjects')
            </div>
            {{-- Project close to deadline --}}
            <div class="tab-pane fade" id="project-close-deadline" role="tabpanel" aria-labelledby="project-tab-close-deadline">
                @include('admin.projectclosetoDeadline')
            </div>
        </div>
    </div>

</x-app-layout>