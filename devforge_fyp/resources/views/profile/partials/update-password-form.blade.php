<section>
    <div class="container-fuild px-md-5 px-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-4">
                <form method="post" action="{{ route('password.update') }}" style="min-height: 70vh;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;" class="w-100 border border-primary-subtle rounded-4 mt-5 p-4"  autocomplete="off">
                    @csrf
                    @method('put')
                    <h4 class="text-primary fw-bold w-100 text-center mb-5 mt-2">Update Password</h4>
                    <p>Ensure your account is using a long, random password to stay secure.</p>
                    @if (session('status') === 'password-updated')
                        <p
                            x-data="{ show: true }"
                            x-show="show"
                            x-transition
                            x-init="setTimeout(() => show = false, 2000)"
                            class="alert alert-success my-2 w-100"
                        >{{ __('You updated password successfully!') }}</p>
                    @endif
                    <div class="form-floating">
                        <input name="current_password" value="{{ old('current_password') }}"  type="password" class="form-control" id="floatingInput" placeholder="" autocomplete="off">
                        <label for="floatingInput">Current Password</label>
                    </div>
                    <x-input-error :messages="$errors->updatePassword->get('current_password')" class="mt-2" />

                    <div class="form-floating mt-4">
                        <input name="password" value="{{ old('password') }}" type="password" class="form-control" id="floatingPassword" placeholder="">
                        <label for="floatingPassword">New Password</label>
                    </div>
                    <x-input-error :messages="$errors->updatePassword->get('password')" class="mt-2" />

                    <div class="form-floating mt-4">
                        <input name="password_confirmation" value="{{ old('password_confirmation') }}" type="password" class="form-control" id="floatingPassword2" placeholder="">
                        <label for="floatingPassword">Confirm Password</label>
                    </div>
                    <x-input-error :messages="$errors->updatePassword->get('password_confirmation')" class="mt-2" />

                    <button type="submit" style="height: 50px;" class="w-100 btn bg-primary mt-4 login-register-btn text-white py-2 rounded-5">SAVE</button>
                </form>
            </div>
        </div>
    </div>
</section>
