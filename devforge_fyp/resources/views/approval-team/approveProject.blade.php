@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/approveprojects.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">Approve Projects</x-slot>
    <div class="container-fluid px-md-5 px-3">
    <div class="row">
        <h2 class="mt-4 fw-bold">Projects for Approval (Director)</h2>
        <div class="table-responsive mt-3">
            @if($groups->isEmpty())
                <p class="mt-4">No projects available for approval.</p>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Group</th>
                            <th>Scoping Document</th>
                            <th>Status</th>
                            <th>Feedback</th>
                            <th>Project status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($groups as $group)
                            <tr>
                                <td class="py-3">{{ $group->project_name }}</td>
                                <td class="py-3">{{ $group->group_id }}</td>
                                <td class="text-primary py-3">
                                    @if($group->scoping_id && $group->scoping_status == 'Yes')
                                        <a class="text-decoration-none" href="{{ route('downloadScopingDocuments', ['scoping_id' => $group->scoping_id]) }}">See Scoping Documents</a>
                                    @else
                                        <a class="text-decoration-none" href="#">Add Scoping Documents</a>
                                    @endif
                                </td>
                                <td class="text-primary py-3">
                                    <div class="d-flex gap-2">
                                        @if($group->approval_status == 'pending' && $group->scoping_status == 'Yes')
                                            {{-- Reject Form --}}
                                            <form method="POST" action="{{ route('approvalProject') }}">
                                                @csrf
                                                <input type="hidden" name="group_id" value="{{ $group->group_id }}">                                    
                                                <input type="hidden" name="status" value="Reject">
                                                <button style="height: 30px;" class="btn btn-secondary text-white rounded-5 d-flex justify-content-center align-items-center px-3" type="submit">Reject</button>
                                            </form>
                                            {{-- Approve Form --}}
                                            <form method="POST" action="{{ route('approvalProject') }}">
                                                @csrf
                                                <input type="hidden" name="group_id" value="{{ $group->group_id }}">                                    
                                                <input type="hidden" name="status" value="Approved">
                                                <button style="height: 30px;" class="btn btn-primary text-white rounded-5 d-flex justify-content-center align-items-center px-3" type="submit">Approve</button>
                                            </form>
                                        @else
                                            @if($group->approval_status == 'Approved')
                                                <span class="text-decoration-none">Approved by Approval Team</span>
                                            @elseif($group->approval_status == 'pending' && $group->scoping_status == 'No')
                                                <span class="text-danger text-decoration-none" style="display: none">Needs to be approved by Approval Team</span>
                                            @else
                                                <span class="text-danger text-decoration-none">Rejected by Approval Team</span>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                                <td class="py-3 text-primary">
                                    @php
                                        $title = $group->project_name;
                                        $team_leader = $group->team_leader;
                                        $user = \App\Models\User::where('enrollment_number', $team_leader)->first();
                                        $emailLink = $user ? $user->email : 'sryders2000@gmail.com';
                                    @endphp
                                    @if($group->approval_status == 'Approved')
                                        <button type="button" class="btn text-primary" data-bs-toggle="modal" data-bs-target="#feedbackModal" data-email="{{ $emailLink }}" data-title="{{ $title }}" data-leader="{{ $team_leader }}">Feedback</button>
                                    @else
                                        <button class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#feedbackModal" style="display: none">Feedback</button>
                                    @endif
                                </td>   
                                <td class="text-primary py-3">
                                    <div class="d-flex gap-2">
                                        @if($group->approval_status == 'Approved' && $group->scoping_status == 'Yes' && $group->project_status == 'ongoing')
                                            <form method="POST" action="{{ route('completeProject') }}">
                                                @csrf
                                                <input type="hidden" name="group_id" value="{{ $group->group_id }}">                                    
                                                <input type="hidden" name="status" value="completed">
                                                <button style="height: 30px;" class="btn btn-primary text-white rounded-5 d-flex justify-content-center align-items-center px-3" type="submit">Completed</button>
                                            </form>
                                        @else
                                            @if($group->project_status == 'completed')
                                                <span class="text-decoration-none">{{ $group->project_status }}</span>   
                                            @else
                                                <span class="text-decoration-none"></span>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>

<!-- Modal feedback -->
@if(!$groups->isEmpty())
<div class="modal fade" id="feedbackModal" tabindex="-1" aria-labelledby="feedbackModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="feedbackModalLabel"></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="{{ route('sendFeedback', ['email' => $emailLink]) }}">
                @csrf
                <input type="hidden" name="recipient_email" id="recipient_email">
                <div class="modal-body">
                    <textarea required class="form-control" name="feedback_message" id="feedback_message" cols="30" rows="7" placeholder="Your Feedback Message"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary text-white">Send Feedback</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var feedbackModal = document.getElementById('feedbackModal');
    feedbackModal.addEventListener('show.bs.modal', function (event) {
        var button = event.relatedTarget;
        var title = button.getAttribute('data-title');
        var leader = button.getAttribute('data-leader');
        var email = button.getAttribute('data-email');
        var modalTitle = feedbackModal.querySelector('.modal-title');
        var recipientEmail = feedbackModal.querySelector('#recipient_email');
        var feedbackForm = feedbackModal.querySelector('form');

        modalTitle.textContent = title;
        recipientEmail.value = email;

        // Update form action with the correct email
        feedbackForm.action = "{{ route('sendFeedback', ['email' => $emailLink]) }}";
    });
</script>
@endif



</x-app-layout>