@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/admindashboard.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">Add Supervisor</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <div class="row mt-4">
            <div class="col-md-3">
                <div class="p-3 mt-5 rounded-2" style="box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;">
                    <h4>Add Supervisor</h4>
                    {{-- Add supervisor form --}}
                    <form method="POST" action="{{ route('createSupervisor') }}"  enctype="multipart/form-data">
                      @csrf
                        <input class="form-control rounded-5 mt-3 mb-2" name="name" type="text" placeholder="Username">
                        <input class="form-control rounded-5 mb-2" name="email" type="email" placeholder="Email Address">
                        <input class="form-control rounded-5 mb-2" name="password" type="text" placeholder="Password">
                        <button class="w-100 rounded-5 btn btn-primary text-white mt-3" type="submit">Add Supervisor</button>
                    </form>
                </div>
            </div>
            <div class="col-md-9">
                <div style="height: 80vh;" class="overflow-y-scroll mt-3">
                    {{-- <table class="table">
                        <thead>
                          <tr>
                            <th>Admin Username</th>
                            <th>Admin Email</th>
                            <th>Admin Password</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>   
                          <tr>
                            <td class="py-3">Sangay Tshering</td>
                            <td class="py-3">Sangay@gmail.com</td>
                            <td class="py-3 overflow-x-scroll">#AhUwhj12345nchwdAhUwhj12345nchwdAhUw</td>
                            <td>
                                {{-- <button data-bs-toggle="modal" action="{{ route('deleteSupervisor', ['id' => $users->id]) }}" data-bs-target="#deleteUserModal" class="btn text-secondary fw-bold" style="font-size: 13px;">Delete</button> --}}
                                {{-- <button data-bs-toggle="modal" data-bs-target="#deleteUserModal" class="btn text-secondary fw-bold" style="font-size: 13px;">Delete</button>
                              </td>
                          </tr>
                        
                        </tbody>
                    </table> --}}

                    <table class="table">
                      <thead>
                          <tr>
                              <th>Admin Username</th>
                              <th>Admin Email</th>
                              <th>Admin Password</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>   
                          @foreach($users as $user)
                          <tr>
                              <td class="py-3">{{ $user->name }}</td>
                              <td class="py-3">{{ $user->email }}</td>
                              <td class="py-3 overflow-x-scroll">{{ $user->password }}</td>
                              <td>
                                  <button type="button" class="btn text-secondary fw-bold delete-user-btn" data-bs-toggle="modal" data-bs-target="#deleteUserModal" data-user-id="{{ $user->id }}" data-user-name="{{ $user->name }}" style="font-size: 13px;">Delete</button>
                              </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
                  
                </div>
            </div>
        </div>
    </div>
  <!-- Delete Modal -->
  {{-- <div class="modal fade" id="deleteUserModal" tabindex="-1" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content border-0" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="deleteUserModalLabel">Delete User</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          Are you sure, you want delete user <span class="text-danger">Sangay Tshering</span>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
          {{-- Delete User form --}}
          {{-- <form action="">
            <button type="submit" class="btn btn-secondary text-white">Yes, Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div> --}}

  <div class="modal fade" id="deleteUserModal" tabindex="-1" aria-labelledby="deleteUserModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0" style="box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="deleteUserModalLabel">Delete User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete user <span class="text-danger" id="userNameToDelete"></span>?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                {{-- Delete User form --}}
                <form id="deleteUserForm" action="{{ route('deleteSupervisor', ['id' => $user->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-secondary text-white">Yes, Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

  <script>
    document.querySelectorAll('.delete-user-btn').forEach(function(btn) {
        btn.addEventListener('click', function() {
            // Get user id and name from data attributes
            var userId = btn.getAttribute('data-user-id');
            var userName = btn.getAttribute('data-user-name');

            // Set user id in the modal form action
            document.getElementById('deleteUserForm').action = "{{ route('deleteSupervisor', ['id' => ':userId']) }}".replace(':userId', userId);

            // Set user name in the modal
            document.getElementById('userNameToDelete').innerText = userName;
        });
    });
</script>

</x-app-layout>