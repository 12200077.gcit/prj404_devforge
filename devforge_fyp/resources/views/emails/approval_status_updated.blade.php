<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project Status Updated By Approval Team</title>
</head>
<body>
    <p>Dear {{ $group->team_leader }},</p>
    <p>Your project status has been updated by the Approval Team.</p>
    <p>New project status: {{ $group->approval_status }}</p>
    <p>Regards,</p>
    <p>The Approval Team</p>
    <a href="https://drive.google.com/file/d/1rudNHY37m6NmuKFFR42aFwS_MAuP2TkF/view?usp=drivesdk"></a>
</body>
</html>
