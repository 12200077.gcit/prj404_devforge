@component('mail::message')
# New Admin Created

Dear {{ $user->name }},

An admin account has been created for you by the approval team.

Here are your login details:

- **Email**: {{ $user->email }}
- **Password**: {{ $password }}

You can log in using the provided credentials.

Thank you.

@endcomponent
