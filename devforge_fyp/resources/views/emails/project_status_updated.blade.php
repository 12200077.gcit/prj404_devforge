<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project Status Updated By Admin</title>
</head>
<body>
    <p>Dear {{ $group->team_leader }},</p>
    <p>Your project status has been updated by the admin.</p>
    <p>New status: {{ $group->admin_status }}</p>
    <p>Regards,</p>
    <p>The Admin</p>
</body>
</html>
