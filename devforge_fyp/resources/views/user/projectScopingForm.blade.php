@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/user.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">Project Scoping Form</x-slot>
    <div class="conatainer-fluid px-md-5 px-3" >
        <div class="row">
            <h2 class="mt-4 fw-bold">Project Scoping Form</h2>
            <div class="col-md-4">
                <div>
                    <h5 class="fw-bold mt-3">Project Overview</h5>
                    <div class="d-flex flex-column gap-0" style="font-size: 12px;">
                        <span>
                            <span class="fw-bold opacity-75">Project Title:</span>
                            <span>{{$project->project_title}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Project Advisor:</span>
                            <span>{{$project->prjoect_advisor}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Project Manager:</span>
                            <span>{{$project->project_manager}}</span>
                        </span>
                    </div>
                </div>
                <div>
                    <h5 class="fw-bold mt-3">Client Details</h5>
                    <div class="d-flex flex-column gap-0" style="font-size: 12px;">
                        <span>
                            <span class="fw-bold opacity-75">Company Name:</span>
                            <span>{{$project->company_name}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">WEB URL:</span>
                            <span>{{$project->web_url}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Telphone/Mobile:</span>
                            <span>{{$project->phone}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Email:</span>
                            <span>{{$project->company_email}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Company Liason Officer (CLO):</span>
                            <span>{{$project->clo}}</span>
                        </span>
                    </div>
                </div>
                <div>
                    <h5 class="fw-bold mt-3">Project Objectives</h5>
                    <p style="font-size: 12px;">
                        {{$project->projectobjectives}}
                    </p>
                </div>
            </div>
            

            <div class="col-md-8">
                <form id="form" style="height: 70vh;" class="border border-dark-subtle rounded-4 p-5" action="{{ route('ScopingForm', ['group_id' => $group_id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    {{-- Scope input show--}}
                    <div id="pageScope">
                        <h5 class="mb-3 fw-bold">Scope</h5>
                        <textarea name="scope" id="scope" class="w-100 rounded-4 p-3" placeholder="Write project deliverables here" rows="8"></textarea>
                    </div>
                    {{-- Deliverables input show--}}
                    <div id="pageDeliverables" style="display: none;">
                        <h5 class="mb-3 fw-bold">Deliverables</h5>
                        <textarea name="deliverables" id="deliverables" class="w-100 rounded-4 p-3" placeholder="Write project deliverables here" rows="8"></textarea>
                    </div>
                    {{-- Technical Requirements input show--}}
                    <div id="pageRequirements" style="display: none;">
                        <h5 class="mb-3 fw-bold">Technical Requirements</h5>
                        <textarea id="requirements" name="requirements" class="w-100 rounded-4 p-3" placeholder="Write project technical Requirements here" rows="8"></textarea>
                    </div>
                    {{-- Sitemap input show--}}
                    <div id="pageSitemap" style="display: none;">
                        <h5 class="mb-3 fw-bold">Sitemap</h5>
                        <textarea id="sitemap" name="sitemap" class="w-100 rounded-4 p-3" placeholder="Write project deliverables here" rows="8"></textarea>
                    </div>
                    {{-- Timeline input show--}}
                    <div id="pageTimeline" style="display: none;">
                        <h5 class="mb-3 fw-bold">Timeline</h5>
                        {{-- <textarea id="deliverables" class="w-100 rounded-4 p-3" placeholder="Write project deliverables here" rows="8"></textarea> --}}

                        <div>
                            <label for="scope" class="col-md-4 col-form-label text-md-right">Gathering Requirements</label>
                            <input type="date" name="gathering_start"  id="gathering_start" class="col-3" required>
                            <input type="date" name="gathering_end"  id="gathering_end" class="col-3" required>
                        </div>

                        <div>
                            <label for="scope" class="col-md-4 col-form-label text-md-right">Analysis</label>
                            <input type="date" name="analysis_start"  id="analysis_start" class="col-3" required>
                            <input type="date" name="analysis_end"  id="analysis_end" class="col-3" required>
                        </div>

                        <div>
                            <label for="scope" class="col-md-4 col-form-label text-md-right">Design</label>
                            <input type="date" name="design_start"  id="design_start" class="col-3" required>
                            <input type="date" name="design_end"  id="design_end" class="col-3" required>
                        </div>

                        <div>
                            <label for="scope" class="col-md-4 col-form-label text-md-right">Coding</label>
                            <input type="date" name="coding_start"  id="coding_start" class="col-3" required>
                            <input type="date" name="coding_end"  id="coding_end" class="col-3" required>
                        </div>

                        <div>
                            <label for="scope" class="col-md-4 col-form-label text-md-right">Testing</label>
                            <input type="date" name="testing_start"  id="testing_start" class="col-3" required>
                            <input type="date" name="testing_end"  id="testing_end" class="col-3" required>
                        </div>

                        <div>
                            <label for="scope" class="col-md-4 col-form-label text-md-right">Deployment</label>
                            <input type="date" name="deployment_start"  id="deployment_start" class="col-3" required>
                            <input type="date" name="deployment_end"  id="deployment_end" class="col-3" required>
                        </div>
                       

                    </div>
                    <div class="w-100 mt-3 d-flex justify-content-between">
                        <button id="prevBtn" type="button" class="btn text-primary" disabled>Previous</button>
                        <button id="nextBtn" type="button" class="btn btn-primary text-white px-5 rounded-4">Next</button>
                    </div>
                    <div class="w-100 d-flex justify-content-center">
                        <button class="btn  px-5 btn-primary text-white" type="submit" id="submitBtn" style="display: none;">Submit</button>
                    </div>

                </form>
                {{-- form page status --}}
                <div id="formStatus" class="w-100 mt-xl-3 mt-2 d-flex gap-2 overflow-x-scroll">
                    <button id="scopeBtn" class="d-flex btn btn-outline-primary rounded-4 gap-2" type="button">
                        <span>Scope</span>
                    </button>
                    <button id="deliverablesBtn" class="d-flex btn btn-outline-primary rounded-4 gap-2" type="button">
                        <span>Deliverables</span>
                    </button>
                    <button id="requirementsBtn" class="d-flex btn btn-outline-primary rounded-4 gap-2" type="button">
                        <span>Technical Requirements</span>
                    </button>
                    <button id="sitemapBtn" class="d-flex btn btn-outline-primary rounded-4 gap-2" type="button">
                        <span>Sitemap</span>
                    </button>
                    <button id="timelineBtn" class="d-flex btn btn-outline-primary rounded-4 gap-2" type="button">
                        <span>Timeline</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        const prevBtn = document.getElementById('prevBtn');
        const nextBtn = document.getElementById('nextBtn');
        const submitBtn = document.getElementById('submitBtn');
        const pages = ['pageScope', 'pageDeliverables', 'pageRequirements', 'pageSitemap', 'pageTimeline'];
        const pageButtons = ['scopeBtn', 'deliverablesBtn', 'requirementsBtn', 'sitemapBtn', 'timelineBtn'];

        let currentPageIndex = 0;

        nextBtn.addEventListener('click', () => {
            const currentPage = document.getElementById(pages[currentPageIndex]);
            const nextPage = document.getElementById(pages[currentPageIndex + 1]);
            const currentPageButton = document.getElementById(pageButtons[currentPageIndex]);
            const nextPageButton = document.getElementById(pageButtons[currentPageIndex + 1]);

            currentPage.style.display = 'none';
            nextPage.style.display = 'block';
            currentPageButton.innerHTML += '<box-icon name="check-circle" type="solid" color="#f48422"></box-icon>';
            nextPageButton.classList.remove('disabled');

            currentPageIndex++;
            prevBtn.disabled = false;

            if (currentPageIndex === pages.length - 1) {
                nextBtn.disabled = true;
                submitBtn.style.display = 'block';

            }
        });

        prevBtn.addEventListener('click', () => {
            const currentPage = document.getElementById(pages[currentPageIndex]);
            const prevPage = document.getElementById(pages[currentPageIndex - 1]);
            const currentPageButton = document.getElementById(pageButtons[currentPageIndex]);
            const prevPageButton = document.getElementById(pageButtons[currentPageIndex - 1]);

            currentPage.style.display = 'none';
            prevPage.style.display = 'block';
            prevPageButton.innerHTML = '<span>' + prevPageButton.innerText + '</span>';

            currentPageIndex--;
            nextBtn.disabled = false;

            if (currentPageIndex === 0) {
                prevBtn.disabled = true;
            }
        });
    </script>
</x-app-layout>





















