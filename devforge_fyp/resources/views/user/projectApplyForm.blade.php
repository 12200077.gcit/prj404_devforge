@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/user.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">Apply Project</x-slot>
    <div class="conatainer-fluid px-md-5 px-3" >
        <div class="row">
            <h2 class="mt-4 fw-bold">Project Apply Form</h2>
            <div class="col-md-5">
                <div>
                    <h5 class="fw-bold mt-3">Project Overview</h5>
                    <div class="d-flex flex-column gap-0" style="font-size: 12px;">
                        <span>
                            <span class="fw-bold opacity-75">Project Title:</span>
                            <span>{{$projects->project_title}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Project Advisor:</span>
                            <span>{{$projects->project_title}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Project Manager:</span>
                            <span>{{$projects->project_manager}}</span>
                        </span>
                    </div>
                </div>
                <div>
                    <h5 class="fw-bold mt-3">Client Details</h5>
                    <div class="d-flex flex-column gap-0" style="font-size: 12px;">
                        <span>
                            <span class="fw-bold opacity-75">Company Name:</span>
                            <span>{{$projects->company_name}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">WEB URL:</span>
                            <span>{{$projects->web_url}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Telphone/Mobile:</span>
                            <span>{{$projects->phone}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Email:</span>
                            <span>{{$projects->company_email}}</span>
                        </span>
                        <span>
                            <span class="fw-bold opacity-75">Company Liason Officer (CLO):</span>
                            <span>{{$projects->clo}}</span>
                        </span>
                    </div>
                </div>
                <div>
                    <h5 class="fw-bold mt-3">Project Objectives</h5>
                    <p style="font-size: 12px;">
                        {{$projects->projectobjectives}}
                    </p>
                </div>
            </div>
            
            <div class="col-md-7">
                <form style="min-height: 70vh;" class="border border-dark-subtle rounded-4 p-5 row" action="{{ route('applyGroup') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="project_id" value="{{ $projects->project_id }}">
                    <input type="hidden" name="project_name" value="{{ $projects->project_title }}">
    
                    <div class="col-md-8" id="members-container">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Team Lead Enrollment Number</label>
                            <input required type="text" class="form-control rounded-5" name="team_leader" id="team_leader">
                        </div>
                        @for ($i = 1; $i <= $projects->students_number-1; $i++)
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Member {{ $i }}, Enrollment Number</label>
                                <input required type="text" class="form-control rounded-5" name="members[]">
                            </div>
                        @endfor
                        <button class="w-100 btn btn-primary text-white rounded-5" type="submit">Submit</button>
                    </div>
                </form>
                
            </div>
        </div>

    </div>
</x-app-layout>





















