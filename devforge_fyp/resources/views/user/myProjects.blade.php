@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/user.css') }}">
@endsection
<x-app-layout>
    <x-slot name="title">My Projects</x-slot>
    <div class="container-fluid px-md-5 px-3">
        <h2 class="mt-4 fw-bold">My Projects</h2>
          <!-- Nav tabs -->
        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
            <li class="nav-item">
                <a style="font-size: 18px;font-weight:600;" class="nav-link active text-dark" id="approval-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">For Approval</a>
            </li>
            <li class="nav-item">
                <a style="font-size: 18px;font-weight:600;" class="nav-link text-dark" id="ongoing-project-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Ongoing Project</a>
            </li>
        </ul>
         <!-- Tab panes -->
        <div style="min-height: 65vh;" class="tab-content border border-top-0 rounded-bottom-5 border-dark-subtl border-opacity-25 p-5">
            <div class="tab-pane fade show active table-responsive" id="home" role="tabpanel" aria-labelledby="approval-tab">
                <table class="table">
                    <thead>
                      <tr>
                        <th>Project</th>
                        <th>Date Applied</th>
                        <th>Admin Approval</th>
                        <th>Scoping Document</th>
                        <th>Final Approval</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @if($groups->isEmpty())
                          <tr>
                              <td colspan="6" class="text-center py-3">No projects available<br><br><br>
                                <a href="{{ url('/') }}" class="btn btn-primary">See More</a>
                              </td>   
                          </tr>
                      @else
                          @foreach($groups as $group)
                              <tr>
                                  <td class="py-3">{{ $group->project_name }}</td>
                                  <td class="py-3">{{ $group->created_at->format('Y-m-d') }}</td>
                                    <td class="py-3 @if ($group->admin_status == 'Reject') text-danger @else text-primary @endif">
                                        {{ $group->admin_status }}
                                    </td>
                                    
                                  <td class="py-3">
                                      @if ($group->admin_status == 'Reject' || $group->admin_status == 'pending')
                                          <span class="text-muted" style="display: none">Form not available</span>
                                      @else
                                            @if ($group->scoping_status == 'Yes')
                                                <span class="text-primary">Scoping Documents Submitted</span>   
                                            @else
                                                <a class="btn btn-secondary text-white rounded-4" href="{{ url('/projectScopingForm/' . $group->group_id) }}">Fill up Form</a>
                                            @endif
                                      @endif
                                  </td>
                                  @if ($group->admin_status == 'Reject' || $group->admin_status == 'pending')
                                      <td class="text-muted py-3" style="display: none"></td>
                                  @else
                                    <td class="py-3 @if ($group->admin_status == 'Reject') text-danger @else text-primary @endif">
                                        {{ $group->approval_status }}
                                    </td>
                                  @endif
                                  <td class="py-3">
                                      {{-- @if ($group->admin_status == 'Reject' || $group->admin_status == 'pending')
                                          <a class="btn btn-primary text-white rounded-4" href="#" style="display: none"></a>
                                      @else
                                          <a class="btn btn-primary text-white rounded-4" href="{{ route('userExports', ['scoping_id' => $group->scoping_id])}}">Export</a>
                                      @endif --}}
                                        @if ($group->approval_status == 'Approved')
                                            <a class="btn btn-primary text-white rounded-4" href="{{ route('userExports', ['scoping_id' => $group->scoping_id])}}">Export</a>
                                        @else
                                            <a class="btn btn-primary text-white rounded-4" href="#" style="display: none"></a>
                                        @endif
                                  </td>
                              </tr>
                          @endforeach
                      @endif
                  </tbody>
                </table>
            </div>

            {{-- Ongoing Project --}}
            <div class="tab-pane fade show active table-responsive" id="profile" role="tabpanel" aria-labelledby="approval-tab">
                <table class="table">
                    <thead>
                      <tr>
                        <th>Project</th>
                        <th>Date Started</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if($groups->isEmpty())
                            <tr>
                                <td colspan="6" class="text-center py-3">No Ongoing projects available<br><br><br>
                                  <a href="{{ url('/') }}" class="btn btn-primary">See More</a>
                                </td>
                            </tr>
                        @else
                        @foreach($groups as $group)
                            @if($group->admin_status == 'Approved' && $group->approval_status == 'Approved')
                                <tr>
                                    <td class="py-3">{{ $group->project_name }}</td>
                                    <td class="py-3">{{ $group->created_at->format('Y-m-d') }}</td>
                                    <td class="py-3 @if ($group->admin_status == 'Reject') text-danger @else text-primary @endif">
                                        {{ $group->admin_status }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach                 
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</x-app-layout>