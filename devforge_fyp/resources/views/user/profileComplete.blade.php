@section('page-css')
    <link rel="stylesheet" href="{{ asset('css/app/user.css') }}">
@endsection
<x-app-layout>
    {{-- Complete Profile --}}
    <div class="container-fluid px-md-5 px-3">
        <div class="row d-flex justify-content-center">
            <div class="col-xl-3 col-md-4 rounded-4 p-4" style="min-height:70vh;margin-top:40px;box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;">
                <h5>Complete your profile</h5>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ route('userProfileComplete') }}" enctype="multipart/form-data">
                    @csrf
                    <input style="font-size: 14px;" class="form-control py-2 rounded-5 mb-3" type="number" name="contact_number" id="contact_number" placeholder="Contact Number">
                    <input style="font-size: 14px;" class="form-control py-2 rounded-5 mb-3" type="number" name="enrollment_number" id="enrollment_number" placeholder="Enrollment No">
                    <input style="font-size: 14px;" class="form-control py-2 rounded-5 mb-3" type="text" name="linkedin" placeholder="LinkedIn Profile Link (Optional)">
                    <input style="font-size: 14px;" class="form-control py-2 rounded-5 mb-3" type="text" name="online" placeholder="Online Portfolio Link (Optional)">
                    {{-- <input type="file" name="image" id="image" style="font-size: 14px;" class="form-control py-2 rounded-5 mb-3"/> --}}
                    <h6 class="fw-bold">Skills</h6>
                    <div class="d-flex flex-wrap gap-2">
                        <div>
                            <input type="checkbox" name="skills[]" class="btn-check rounded-5" id="web-dev-checkbox" value="Website Development" autocomplete="off">
                            <label id="web-dev-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="web-dev-checkbox">Website Development</label>
                        </div>
                        <div>
                            <input type="checkbox" name="skills[]" class="btn-check rounded-5" id="mobile-app-checkbox" value="Mobile App" autocomplete="off">
                            <label id="mobile-app-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="mobile-app-checkbox">Mobile App</label>
                        </div>
                        <div>
                            <input type="checkbox" name="skills[]" class="btn-check rounded-5" id="ui-ux-checkbox" value="UI/UX" autocomplete="off">
                            <label id="ui-ux-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="ui-ux-checkbox">UI/UX</label>
                        </div>
                        <div>
                            <input type="checkbox" name="skills[]" class="btn-check rounded-5" id="content-strategy-checkbox" value="Content Strategy" autocomplete="off">
                            <label id="content-strategy-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="content-strategy-checkbox">Content Strategy</label>
                        </div>
                        <div>
                            <input type="checkbox" name="skills[]" class="btn-check rounded-5" id="project-management-checkbox" value="Project Management" autocomplete="off">
                            <label id="project-management-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="project-management-checkbox">Project Management</label>
                        </div>
                        <div>
                            <input type="checkbox" name="skills[]" class="btn-check rounded-5" id="digital-checkbox" value="Digital Marketing" autocomplete="off">
                            <label id="project-management-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="digital-checkbox">Digital Marketing</label>
                        </div>
                    </div>
                    <h6 class="fw-bold mt-3">Field of Interest</h6>
                    <div class="d-flex flex-wrap gap-2">
                        <div>
                            <input type="checkbox" name="interest" class="btn-check rounded-5" id="web-dev-i-checkbox" value="Website Development" autocomplete="off">
                            <label id="web-dev-i-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="web-dev-i-checkbox">Website Development</label>
                        </div>
                        <div>
                            <input type="checkbox" name="interest" class="btn-check rounded-5" id="mobile-app-i-checkbox" value="Mobile App" autocomplete="off">
                            <label id="mobile-app-i-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="mobile-app-i-checkbox">Mobile App</label>
                        </div>
                        <div>
                            <input type="checkbox" name="interest" class="btn-check rounded-5" id="others-checkbox" value="Others" autocomplete="off">
                            <label id="Others-label" style="font-size: 12px;padding:2px;" class="border btn border-dark-subtle rounded-5 px-2" for="others-checkbox">Others</label>
                        </div>
                    </div>
                    <button class="w-100 btn btn-primary text-white mt-4 rounded-5" type="submit">Complete</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        const checkboxes = document.querySelectorAll('.btn-check');
        checkboxes.forEach(function(checkbox) {
            checkbox.addEventListener('change', function() {
                const label = this.nextElementSibling;
                if (this.checked) {
                    label.classList.add('bg-primary');
                    label.classList.add('text-white');
                } else {
                    label.classList.remove('bg-primary');
                    label.classList.remove('text-white');
                }
            });
        });
    </script>
</x-app-layout>