<x-guest-layout>
    <x-slot name="title">Register</x-slot>
    <div class="container-fuild px-md-5 px-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-4">
                <form method="POST" action="{{ route('register') }}"  style="min-height: 70vh;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;" class="w-100 border border-primary-subtle rounded-4 mt-5 p-4" autocomplete="off">
                    @csrf
                    <h4 class="text-primary fw-bold w-100 text-center mb-4">Create new account</h4>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-floating mb-3">
                        <input name="name" value="{{ old('name') }}" type="text" class="form-control" id="floatingName" placeholder="name@example.com" autocomplete="off">
                        <label for="floatingName">Name</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input name="email" value="{{ old('email') }}" type="email" class="form-control" id="floatingInput" placeholder="name@example.com" autocomplete="off">
                        <label for="floatingInput">Email address</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input name="password" value="{{ old('password') }}" type="password" class="form-control" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                    </div>
                    <div class="form-floating mb-4">
                        <input name="password_confirmation" value="{{ old('password_confirmation') }}" type="password" class="form-control" id="floatingConfrimPassword" placeholder="Confirm Password">
                        <label for="floatingConfrimPassword">Confirm Password</label>
                    </div>
                    <button type="submit" style="height: 50px;" class="w-100 btn bg-primary login-register-btn text-white py-2 rounded-5">Register</button>
                    <p class="mt-3 text-center">Already have an account? <a class="text-primary" href="{{ url('/login') }}">Login</a></p>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
