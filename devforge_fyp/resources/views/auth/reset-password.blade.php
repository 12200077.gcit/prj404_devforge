<x-guest-layout>
    <x-slot name="title">Reset Password</x-slot>
    <div class="container-fuild px-md-5 px-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-4">
                <form method="POST" action="{{ route('password.store') }}" style="min-height: 70vh;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;" class="w-100 border border-primary-subtle rounded-4 mt-5 p-4"  autocomplete="off">

                    @csrf
                    <h6 class="mt-3 mb-4 fw-bold text-center">Reset your password</h6>

                    @if (session()->has('status'))
                        <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 6000)" x-show="show" class="row alert alert-success rounded px-3 py-2">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- Password Reset Token -->
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">
                    <div class="form-floating mb-2">
                        <input name="email" required type="email" class="form-control" id="floatingInput" placeholder="xxxnx@gmail.com" autocomplete="off">
                        <label for="floatingInput">Email address</label>
                    </div>
                    @if ($errors->has('email'))
                        <div class="mt-2">
                            <ul class="list-unstyled">
                                @foreach ($errors->get('email') as $error)
                                    <li class="text-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-floating mb-2">
                        <input name="password" name="password" required type="password" class="form-control" id="floatingInput" placeholder="xxxnx@gmail.com" autocomplete="off">
                        <label for="floatingInput">New Password</label>
                    </div>
                    @if ($errors->has('password'))
                        <div class="mt-2">
                            <ul class="list-unstyled">
                                @foreach ($errors->get('password') as $error)
                                    <li class="text-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-floating mb-4">
                        <input  name="password_confirmation" required type="password" class="form-control" id="floatingInput" placeholder="xxxnx@gmail.com" autocomplete="off">
                        <label for="floatingInput">Confirm Password</label>
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <div class="mt-2">
                            <ul class="list-unstyled">
                                @foreach ($errors->get('password_confirmation') as $error)
                                    <li class="text-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <button type="submit" style="height: 50px;" class="w-100 btn bg-primary login-register-btn text-white py-2 rounded-5">Reset Password</button>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
