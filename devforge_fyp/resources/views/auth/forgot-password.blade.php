<x-guest-layout>
    <x-slot name="title">Forgot Password</x-slot>
    <div class="container-fuild px-md-5 px-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-4">
                <form method="POST" action="{{ route('password.email') }}" style="min-height: 70vh;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;" class="w-100 border border-primary-subtle rounded-4 mt-5 p-4"  autocomplete="off">
                    @csrf
                    <p>Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.</p>
                    @if ($errors->has('email'))
                        <div class="mt-2">
                            <ul class="list-unstyled">
                                @foreach ($errors->get('email') as $error)
                                    <li class="text-danger">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('status'))
                        <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 6000)" x-show="show" class="row alert alert-success rounded px-3 py-2">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-floating mb-5">
                        <input name="email" required type="email" class="form-control" id="floatingInput" placeholder="xxxnx@gmail.com" autocomplete="off">
                        <label for="floatingInput">Email address</label>
                    </div>
                    <button type="submit" style="height: 50px;" class="w-100 btn bg-primary login-register-btn text-white py-2 rounded-5">Send a Mail</button>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
