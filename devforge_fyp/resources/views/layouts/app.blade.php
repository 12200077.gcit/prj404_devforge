<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $title ?? 'Devforge' }}</title>
        <link rel="icon" type="image/png" href="{{asset('images/weblogo.png')}}">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('boostrap/main.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app/general.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/quill@2.0.1/dist/quill.snow.css" rel="stylesheet" />
        @yield('page-css')

        <!-- Scripts -->
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

        {{-- Icons --}}
        <script src="https://unpkg.com/boxicons@2.1.4/dist/boxicons.js"></script>

        {{-- toast message cnd --}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

        <style>
            /* Custom style for success toastr */
            .toast-success {
                opacity: 100% !important;
            }
        </style>

    </head>
    <body>
        @if(Auth::user()->role === 'approvalteam')
            @include('layouts.approvalteamNavbar')
        @elseif(Auth::user()->role === 'admin')
            @include('layouts.adminNavbar')
        @else
            @include('layouts.userNavbar')
        @endif
    
        <main>
            {{ $slot }}
        </main>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
          toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": true,
              "positionClass": "toast-top-center",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
  
          @if(Session::has('success'))
              toastr.success("{{ Session::get('success') }}", "Success");
          @endif
  
          @if(Session::has('error'))
              toastr.error("{{ Session::get('error') }}", "Error");
          @endif
      </script>
    </body>
</html>
