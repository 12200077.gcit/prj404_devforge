<nav class="navbar navbar-expand-lg bg-white border-bottom border-dark-subtle position-sticky top-0" style="z-index: 999;">
    <div class="container-fluid px-md-5 px-3">
      <a class="navbar-brand" href="{{ url('/') }}">
        <img height="45" src="{{ asset('images/logo.png') }}" alt="logo">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav ms-auto me-auto mb-2 mb-lg-0 gap-3">
          <li class="nav-item">
            <a class="nav-link {{ request()->is('aboutus*') ? 'active-nav' : '' }}" href="{{ url('/aboutus') }}">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->is('contactus*') ? 'active-nav' : '' }}" href="{{ url('/contactus') }}">Contact Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ request()->is('completedProjects*') ? 'active-nav' : '' }}" href="{{ url('/completedProjects') }}">Completed Projects</a>
          </li>
        </ul>
        <div class="navbar-nav gap-3">
            <li class="nav-item">
                <a class="nav-link bg-secondary rounded-5 px-4 text-white" href="{{ url('/myProject') }}">My Projects</a>
            </li>
            <li class="nav-item dropdown">
              <span class="text-primary">Hi, {{ Auth::user()->name }}</span>
              <button data-bs-toggle="dropdown" aria-expanded="false" class="btn btn-primary rounded-circle p-0 text-uppercase text-white" style="width: 40px; height: 40px;">
                  {{ implode('', array_map(function($name) { return strtoupper($name[0]); }, explode(' ', Auth::user()->name))) }}
              </button>
                <ul class="dropdown-menu  border border-primary">
                  <li><a class="dropdown-item" href="{{ route('editProfile', ['id' => Auth::user()->id]) }}">Edit Details</a></li>
                  <li><a class="dropdown-item" href="{{ url('/profile') }}">Change Password</a></li>
                  <li>
                     <form  method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit" class="text-primary dropdown-item mt-3 bg-transparent border-0">Logout</button>
                     </form>
                  </li>
                </ul>
            </li>
        </div>
      </div>
    </div>
</nav>