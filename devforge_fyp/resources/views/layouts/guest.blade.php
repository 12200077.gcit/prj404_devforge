<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $title ?? 'Devforge' }}</title>
        <link rel="icon" type="image/png" href="{{asset('images/weblogo.png')}}">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('boostrap/main.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/guest/general.css') }}">


        <!-- Scripts -->
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

        {{-- Icons --}}
        <script src="https://unpkg.com/boxicons@2.1.4/dist/boxicons.js"></script>

        {{-- toast message cnd --}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
        <style>
            .toast-success {
                opacity: 100% !important;
            }
        </style>
    </head>
    <body class="overflow-x-hidden">
      {{-- If logged in --}}
      @if(Auth::check())
        @if(Auth::user()->role === 'approvalteam')
            @include('layouts.approvalteamNavbar')
        @elseif(Auth::user()->role === 'admin')
            @include('layouts.adminNavbar')
        @else
            @include('layouts.userNavbar')
        @endif
        
      {{-- If not logged in --}}
      @else
        <nav style="z-index: 9999;" class="navbar navbar-expand-lg bg-white border-bottom border-dark-subtle position-sticky top-0">
          <div class="container-fluid px-md-5 px-3">
            <a class="navbar-brand" href="{{ url('/') }}">
              <img height="45" src="{{ asset('images/logo.png') }}" alt="logo">
            </a>
            <button class="navbar-toggler border border-primary p-0 " type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <box-icon name='menu' size="lg" color='#74b444' ></box-icon>              
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav ms-auto me-auto mb-2 mb-lg-0 gap-md-3">
                <li class="nav-item">
                  <a class="nav-link {{ request()->is('aboutus*') ? 'active-nav' : '' }}" href="{{ url('/aboutus') }}">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ request()->is('contactus*') ? 'active-nav' : '' }}" href="{{ url('/contactus') }}">Contact Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ request()->is('completedProjects*') ? 'active-nav' : '' }}" href="{{ url('/completedProjects') }}">Completed Projects</a>
                </li>
              </ul>
              <div class="navbar-nav gap-3">
                  <li class="nav-item">
                      <a class="nav-link {{ request()->is('login*') ? 'login-active' : '' }} bg-primary rounded-5 px-4 text-white login-register-btn " href="{{ url('/login') }}">Login</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link {{ request()->is('register*') ? 'login-active' : '' }} bg-primary rounded-5 px-4 text-white login-register-btn" href="{{ url('/register') }}">Register</a>
                  </li>
              </div>
            </div>
          </div>
        </nav>
      @endif
        <main>
            {{ $slot }}
        </main>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
          toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": true,
              "progressBar": true,
              "positionClass": "toast-top-center",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          };
  
          @if(Session::has('success'))
              toastr.success("{{ Session::get('success') }}", "Success");
          @endif
  
          @if(Session::has('error'))
              toastr.error("{{ Session::get('error') }}", "Error");
          @endif
      </script>
    </body>
</html>
