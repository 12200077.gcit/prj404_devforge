<!DOCTYPE html>
<html>
<head>
    <title>Scoping Document PDF</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>
<body>
    <h1>{{ $title }}</h1>
    <p>Team Leader Name: {{ $leaderName;}}</p>
    <p>Enrollment Number: {{ $enrollment }}</p>
    <hr>
    <h2>Scope</h2>
    <p>{{ $scopingdocuments->scope }}</p>
    <h2>Deliverables</h2>
    <p>{{ $scopingdocuments->deliverables }}</p>
    <h2>Requirements</h2>
    <p>{{ $scopingdocuments->requirements }}</p>
    <h2>Sitemaps</h2>
    <p>{{ $scopingdocuments->sitemap }}</p>
</body>
</html>
